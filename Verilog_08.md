# Verilog入门（8）:小结

本节小结前面介绍的Verilog关键字与结构。


#### module

Verilog语言用模块（module）来表示层次化的电路结构。

“module”包含“描述性语句与表达式”和子“module”。

module的声明方式及其端口表示方式在Verilog_01.md中介绍，module的引用方式和parameter关键字在Verilog_05.md中介绍。

#### wire和reg

Verilog有两类数据类型：net和variables。其中net类型我们先学习wire，variables类型我们先学习reg。

用wire表示一条线，用assign关键字与各种表达式表示组合逻辑电路（Verilog_02.md）。

用reg和always结构表示一个D触发器和描述时序电路的行为（Verilog_03.md）。

#### Vector和Array

（Verilog_02.md）

“wire [7:0] addr;”表示由8条线组成的线束。“reg [7:0] data;”表示8位D触发器组成的寄存器。这个概念称为Vector。

“wire addrs[0:7];”表示独立的8条线。“reg data[0:7];”表示独立的8个D触发器。这个概念称为Array。

“wire [7:0] addrs[0:7];”表示8束线束，每束由8条线组成。“reg [7:0] data[0:7];”表示8个8位D触发器组成的寄存器。


#### 常数的表达方式

（Verilog_02.md）
- 4'b0101、8'h12、8'd200：表示若干比特的无符号整数；
- {2'b01, data[5:0]}：表示拼接；
- localparam：给常数起个名字；（Verilog_05.md）

#### 表达式和运算符

（Verilog_02.md）
- 加减：+、-；
- 移位：<<、>>；
- 位运算：&、|、~、^；
- 逻辑运算：&&、||；
- 问号表达式：<条件表达式> ? <为真时结果> : <为假时结果>
- if结构；（Verilog_03.md）

#### 所有的运算符一览

规范4.1 table 9：

![运算符一览](images/table9_operators_1.png)

![运算符一览_续](images/table9_operators_2.png)

#### CASE语句和有限状态机（FSM）

Verilog_06.md。

#### 自动生成类似代码段

generate结构和for结构。

Verilog_07.md。
