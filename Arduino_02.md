# 编译、链接、烧写（2）:芯片上的Hardware Loader

上节介绍PC上的Arduino IDE开发环境搭建。本节介绍FPGA芯片上的用于程序下载的Hardware Loader模块。

Hardware Loader模块的源代码来自顾长怡先生的书籍“基于FPGA与RISC-V的嵌入式系统设计”及其配套开源代码。



### FPGA硬件设计

复制工程03_tinycore_step05到04_arduino_step01，修改工程文件名字。

#### HWLoader模块

HWLoader模块的源代码包含7个文件，放在src的HW_Loader子目录下。

把它们都加入到工程中。


#### GoWin的工程选项设置

由于HWLoader模块的源代码文件用System Verilog语言编写，需要配置一下GOWIN软件。

菜单“Project/Configuration”，选择“Synthesize”页面：“Verilog Language”选项选择“System Verilog 2017”，点“Apply”。


#### 将top模块中的ROM和RAM改为双口RAM

双口RAM是有两个相互独立、可同时对存储单元进行读写的读写端口的RAM模块。

将ROM和RAM改为双口RAM，HWLoader从双口RAM的A口对存储单元进行读写，core从B口对存储单元进行读写。

菜单“Tools/IP Core Generator”，选择“Memory/Block Memory/DPB”，双击DPB。

A口和B口都设置“Address Depth = 1024”、“Data Width = 32”。即4kB的容量。（1k*32bits）

修改rom_data.mi，将Address_depth一行改为：“#Address_depth=1024”。

选择Memory Initialization File为rom_data.mi，点OK。加入工程。


在top.v中加入对双口RAM的引用，作为指令ROM和Data RAM：（注释掉原来的指令ROM实例和数据RAM实例）
```
	wire [31:0] debug_rom_doa;
	wire [31:0] debug_ram_doa;
	wire [31:0] debug_doa;
	wire [31:0] debug_rom_dia;
	wire [21:0] debug_rom_addra_rd;	
	wire [21:0] debug_rom_addra_wr;	
	wire debug_rom_rea;
	wire debug_rom_wea;
```
```
    Gowin_DPB I_ROM(
        .clka(clk_200MHz), //input clka
        .ocea(1'b1), //input ocea
        .cea(debug_rom_cea), //input cea
        .reseta(~reset_n), //input reseta
        .wrea(debug_rom_wea), //input wrea
        .ada(debug_rom_addra[9:0]), //input [9:0] ada
        .douta(debug_rom_doa), //output [31:0] douta
        .dina(debug_rom_dia), //input [31:0] dina

        .clkb(clk_200MHz), //input clkb
        .oceb(ibus_re), //input oceb
        .ceb(1'b1), //input ceb
        .resetb(~reset_n), //input resetb
        .wreb(1'b0), //input wreb
        .adb(ibus_addr[9:0]), //input [9:0] adb
        .doutb(instruction), //output [31:0] doutb
        .dinb(32'b0) //input [31:0] dinb
    );  
```
```
    Gowin_DPB D_RAM(
        .clka(clk_200MHz), //input clka
        .ocea(1'b1), //input ocea
        .cea(debug_ram_cea), //input cea
        .reseta(~reset_n), //input reseta
        .wrea(debug_rom_wea), //input wrea
        .ada(debug_rom_addra[9:0]), //input [9:0] ada
        .douta(debug_ram_doa), //output [31:0] douta
        .dina(debug_rom_dia), //input [31:0] dina

        .clkb(clk_200MHz), //input clkb
        .oceb(1'b1), //input oceb
        .ceb(ram_ce), //input ceb
        .resetb(~reset_n), //input resetb
        .wreb(ram_wre), //input wreb
        .adb(ram_addr), //input [9:0] adb
        .doutb(ram_data_in), //output [31:0] doutb
        .dinb(ram_data_out) //input [31:0] dinb
    );  
```

地址分配：在本设计中，把Memory Map规定为：
- rom地址范围（以字节为单位） [0x????2000 - 0x????2fff]  （以字为单位就是[0x800-bff]）
- ram地址范围（以字节为单位） [0x????0400 - 0x????13ff]  （以字为单位就是[0x100-4ff]）
- ram地址范围（以字节为单位） [0x????0000 - 0x????03ff]  （以字为单位就是[0x000-0ff]）

在Arduino IDE环境的链接配置文件.ld中，配置指令地址从0x80002000开始，数据从0x00000400开始：
```
  PROVIDE (__executable_start = SEGMENT_START("text-segment", 0x80002000)); . = SEGMENT_START("text-segment", 0x80002000);

   . = SEGMENT_START("data-segment", 0x00000400);
  
```
因此，对于HWLoader给出的地址信号（以字为单位），地址译码电路（rom和ram的片选信号）如下：
```
    wire [21:0] debug_rom_addra = debug_rom_wea ? debug_rom_addra_wr : debug_rom_addra_rd;    
    wire debug_rom_cea = (debug_rom_addra[13:11]==3'b001)&(debug_rom_rea|debug_rom_wea);    
    wire debug_ram_cea = (debug_rom_addra[13:11]==3'b000)&(debug_rom_rea|debug_rom_wea); 
    assign debug_doa = debug_rom_cea ? debug_rom_doa :
                         debug_ram_cea ? debug_ram_doa : 32'b0;
```



#### 在top模块中加入对HWLoader模块的引用

HWLoader的顶层模块是debug_coprocessor_wrapper：
```
    wire debug_TXD;	
    wire debug_txd_sel;	
    wire HW_Loader_Reset;		

    debug_coprocessor_wrapper #(.BAUD_PERIOD (234)) hwloader(    
        .clk(sys_clk),  //27MHz
        .reset_n(reset_n),    
        .RXD(uart0_rxd),
        .TXD(debug_TXD),     
        .debug_uart_tx_sel_ocd1_cpu0(debug_txd_sel),   
        .pram_read_enable_in(1'b1),    
        .pram_read_enable_out(debug_rom_rea),    
        .pram_read_addr_out(debug_rom_addra_rd),    
        .pram_read_data_in(debug_doa),    
        .pram_write_enable_out(debug_rom_wea),    
        .pram_write_addr_out(debug_rom_addra_wr),     
        .pram_write_data_out(debug_rom_dia),    
        .cpu_reset(HW_Loader_Reset)
    );  
```

其端口主要分为3类：
- 和rom及ram接口的存储器总线端口；
- 和串口关联的端口；
- clk和reset、cpu_reset；

串口的输入（uart0_rxd）可以同时接HWLoader和peripheral，但输出不行，需要接个mux：
```
    assign uart0_txd = debug_txd_sel?debug_TXD:peri_txd;    
```
mux由HWLoader的端口debug_txd_sel控制。

修改peripheral模块的txd和led端口的连接。
```
    wire [5:0] peri_led;
    wire peri_txd;    

    peripheral m_peripheral(
        .clk(sys_clk),
        .reset_n(reset_n),
        .button(button[0]),
        .led(peri_led),
        .rxd(uart0_rxd),
        .txd(peri_txd),
```

core的reset端口原来直接接输入引脚，现在要加上HWLoader的控制端口：
```
    assign core_RST_N = (~HW_Loader_Reset) & reset_n;	
```

```
    wire core_RST_N;
    core m_core (
        .core_clk(core_clk),
        .reset_n(core_RST_N),
...
```

#### 在core模块中修改ram和peripheral的地址译码电路

core.v中进行以下修改。

- 端口中修改ram_addr的位数，由9位改为10位：
```
module core (
...
    output wire [9:0] ram_addr, //address bus
...
```

- 修改ram和peripheral的地址译码电路：
```
    //to use the 12bit signed imm offset to address the data for optimization of the code
    //assign addr for peripheral at         0x????0000 - 0x????03ff (1kB)
    //assign addr for data ram at           0x????0400 - 0x????13ff (4kB)
//    assign ram_ce = (loadstore_addr[31:10] == 22'b0) & (opcode_LOAD | opcode_STORE);
    assign peripheral_ce = (loadstore_addr[15:10] == 6'b0) & (opcode_LOAD | opcode_STORE);
    assign ram_ce = (loadstore_addr[15:13] == 3'b0) & (loadstore_addr[12:10] != 3'b0) & (opcode_LOAD | opcode_STORE);

```

- 修改监视数据：（查看指令地址（pc[3:2]）和指令[5:2]）
```
    assign monitor_port = {ibus_addr[1:0], instruction[5:2]};
```


### 软件设计

#### arduino .ino代码
在Arduino IDE中输入如下代码：（例子中是保存为Blink1.ino文件）
```
void setup() {
  // put your setup code here, to run once:
}

void loop() {
  // put your main code here, to run repeatedly: 
  unsigned int i;
  asm volatile(
    "addi x16,x16,1;"
  );
  
  for (i=0;i<10;i++) {
    asm volatile(
      "nop;"
    );
  }
}
```
编译。


#### 通过反汇编查看elf内容

为了准确掌握写入指令ROM的内容，需要懂得反汇编工具的使用。

riscv-gcc工具链中有个文件riscv-none-elf-objdump.exe，它的-D选项可用于反汇编.elf文件或.o文件。

在Arduino完成编译后，从日志中查出编译目录，从中复制.elf文件出来。然后编写一个.bat文件，内容示例：
```
C:\Arduino\hardware\riscv\tangnano9k\tools\gcc\bin\riscv-none-elf-objdump.exe -D Blink1.ino.elf  > code.txt
```

然后就可以在code.txt中查看反汇编结果。其内容大致包含如下部分：（注：下面记录生成时代码段的起始位置是0x80000000）

- _start函数：
```
Disassembly of section .text:

80000000 <_start>:
80000000:	00002197          	auipc	gp,0x2
80000004:	d4018193          	addi	gp,gp,-704 # 80001d40 <__global_pointer$>
80000008:	00001517          	auipc	a0,0x1
8000000c:	53c50513          	addi	a0,a0,1340 # 80001544 <completed.1>
80000010:	82018613          	addi	a2,gp,-2016 # 80001560 <_end>
80000014:	40a60633          	sub	a2,a2,a0
80000018:	00000593          	li	a1,0
8000001c:	270000ef          	jal	ra,8000028c <memset>
80000020:	00000517          	auipc	a0,0x0
80000024:	17c50513          	addi	a0,a0,380 # 8000019c <__libc_fini_array>
80000028:	12c000ef          	jal	ra,80000154 <atexit>
8000002c:	1cc000ef          	jal	ra,800001f8 <__libc_init_array>
80000030:	00012503          	lw	a0,0(sp)
80000034:	00410593          	addi	a1,sp,4
80000038:	00000613          	li	a2,0
8000003c:	104000ef          	jal	ra,80000140 <main>
80000040:	1280006f          	j	80000168 <exit>
```
_start函数先设置堆栈指针gp也就是sp的值，然后调用若干函数初始化变量，然后跳转main函数。

- main函数部分：
```
80000124 <_Z5setupv>:
80000124:	00008067          	ret

80000128 <_Z4loopv>:
80000128:	00180813          	addi	a6,a6,1
8000012c:	00a00793          	li	a5,10
80000130:	00000013          	nop
80000134:	fff78793          	addi	a5,a5,-1
80000138:	fe079ce3          	bnez	a5,80000130 <_Z4loopv+0x8>
8000013c:	00008067          	ret

80000140 <main>:
80000140:	ff010113          	addi	sp,sp,-16
80000144:	00112623          	sw	ra,12(sp)
80000148:	fddff0ef          	jal	ra,80000124 <_Z5setupv>
8000014c:	fddff0ef          	jal	ra,80000128 <_Z4loopv>
80000150:	ffdff06f          	j	8000014c <main+0xc>
```

这是对应.ino文件的内容。

- 其它函数还包括：deregister_tm_clones、register_tm_clones、__do_global_dtors_aux、frame_dummy、atexit、exit、__libc_fini_array、__libc_init_array、memset、__register_exitproc、__call_exitprocs、_exit。

总共代码量是0x052c，约2.4kB。

另外还占用了约2点多kB的数据区。



#### 修改crt0.o和crtbegin.o

上面的许多代码是c和c++语言中用来处理变量初始化、赋初值等问题的，可以尝试简化掉。

经搜索和尝试，主要须简化的文件是“tools\gcc\riscv-none-elf\lib”下的crt0.o。

- 将原来的crt0.o改名为crt0_org.o；

- 编写crt0.c：
```
extern int main();

void _start() {
  asm volatile(
    "lui	sp,0x00001;"
	);
  main();
}

void _fini() {
  while(1) {
  }
}
```
该段代码的作用只留下设置sp和调用main。

- 编写编译脚本（gcc_crt0.bat）：
```
"..\..\bin\riscv-none-elf-gcc"  -W -c --specs=nosys.specs -Os -fdata-sections -ffunction-sections -fno-exceptions -fno-unwind-tables  -D__GXX_EXPERIMENTAL_CXX0X__ -DIDE=Arduino "-I..\include" -march=rv32i -mabi=ilp32  "crt0.c" -o "crt0.o"
```
运行脚本，生成新的crt0.o。

另一个要修改的是：“tools\gcc\lib\gcc\riscv-none-elf\12.1.0\rv32i\ilp32”下的crtbegin.o。

将其改名为crtbegin_org.o，然后复制crtend.o并改名为crtbegin.o。（因为crtend.o里面没有代码）


#### 查看新代码

在Arduino中编译。复制elf，反汇编。结果为：
```
80002000 <_start>:
80002000:	00001137          	lui	sp,0x1
80002004:	0240006f          	j	80002028 <main>

80002008 <_fini>:
80002008:	0000006f          	j	80002008 <_fini>

8000200c <_Z5setupv>:
8000200c:	00008067          	ret

80002010 <_Z4loopv>:
80002010:	00180813          	addi	a6,a6,1
80002014:	00a00793          	li	a5,10
80002018:	00000013          	nop
8000201c:	fff78793          	addi	a5,a5,-1
80002020:	fe079ce3          	bnez	a5,80002018 <_Z4loopv+0x8>
80002024:	00008067          	ret

80002028 <main>:
80002028:	ff010113          	addi	sp,sp,-16 # ff0 <__global_pointer$+0x3ec>
8000202c:	00112623          	sw	ra,12(sp)
80002030:	fddff0ef          	jal	ra,8000200c <_Z5setupv>
80002034:	fddff0ef          	jal	ra,80002010 <_Z4loopv>
80002038:	ffdff06f          	j	80002034 <main+0xc>

Disassembly of section .eh_frame:

00000400 <__FRAME_END__>:
 400:	0000                	.2byte	0x0
```
总共才占用0x3c大小的字节数。



### 运行观察

插上模块。

用gowin综合、烧写04_arduino_step01工程。

#### 未下载程序前

Arduino未下载程序前，core运行的程序是rom_data.mi中的指令。

观察led的值：
```
00, 0000
||: （循环）
01, 0100
10, 1000
11, 1000
00, 1011
:||
```

和以下指令（addr[3:2], code[5:2]）是一一对应的：
```
00002783
00178813
01002023
81002823
ff5ff06f
```

#### 下载程序

在Arduino IDE中，菜单“工具/端口”选择串口。

点击“编译（验证）”图标旁边的右箭头图标“上传”。日志后面的部分为：
```
C:\Arduino\hardware\riscv\tangnano9k/tools/reindeer_config --reset --run --port=COM1 --baud=115200 --image=C:\Users\ThinkPad\AppData\Local\Temp\arduino_build_33711/Blink1.ino.hex 
===============================================================================
# Copyright (c) 2019, PulseRain Technology LLC 
# Reindeer Configuration Utility, Version 2.1
===============================================================================
baud_rate  =  115200
com_port   =  COM1
toolchain  =  riscv-none-embed-
===============================================================================
Reseting CPU ...
Loading  C:\Users\ThinkPad\AppData\Local\Temp\arduino_build_33711/Blink1.ino.hex

===================> start the CPU, entry point = 0x80002000
```

#### 下载程序后的现象

观察led的值：
```
00, 1101
01, 1011
10, 0100
11, 1000
00, 1011
||: （循环）
11, 1001
00, 0100
01, 0100
10, 0100
11, 0100
00, 1000
01, 1001
10, 0100
11, 1000
00, 1011
:||
```

对照反汇编指令，是一致的：
```
80002000 <_start>:
80002000:	00001137          	lui	sp,0x1
80002004:	0240006f          	j	80002028 <main>
80002028 <main>:
80002028:	ff010113          	addi	sp,sp,-16 # ff0 <__global_pointer$+0x3ec>
8000202c:	00112623          	sw	ra,12(sp)
80002030:	fddff0ef          	jal	ra,8000200c <_Z5setupv>
||: （循环）
8000200c <_Z5setupv>:
8000200c:	00008067          	ret
80002010 <_Z4loopv>:
80002010:	00180813          	addi	a6,a6,1
80002014:	00a00793          	li	a5,10
80002018:	00000013          	nop
8000201c:	fff78793          	addi	a5,a5,-1
80002020:	fe079ce3          	bnez	a5,80002018 <_Z4loopv+0x8>
80002024:	00008067          	ret
80002028 <main>:
80002028:	ff010113          	addi	sp,sp,-16 # ff0 <__global_pointer$+0x3ec>
8000202c:	00112623          	sw	ra,12(sp)
80002030:	fddff0ef          	jal	ra,8000200c <_Z5setupv>
:||

```
由于没有实现ret指令和bnez指令，所以只有jal和j进行了跳转，其它都是顺序执行。


