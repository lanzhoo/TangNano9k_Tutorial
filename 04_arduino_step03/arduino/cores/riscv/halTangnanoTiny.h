/*+***********************************
Filename: halTangnanoTiny.h
Description: simple hal layer function header.
Modification:
2022.11.28 hzheng creation
************************************-*/

#ifndef _HAL_TANGNANO_TINY_H
#define _HAL_TANGNANO_TINY_H

#ifdef  __cplusplus
extern "C" {
#endif

extern void halSetLed(volatile unsigned int vData);
extern void halUartSend(volatile unsigned int vData);
extern void halDelay(unsigned int vData);

#ifdef  __cplusplus
}
#endif

#endif
