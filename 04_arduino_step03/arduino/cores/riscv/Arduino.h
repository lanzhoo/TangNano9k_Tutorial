/*+***********************************
Filename: Arduino.h
Description: simple arduino header.
Modification:
2022.11.14 hzheng creation
************************************-*/

#ifndef _ARDUINO_H
#define _ARDUINO_H

#ifdef  __cplusplus
extern "C" {
#endif

extern void setup();
extern void loop();

#ifdef  __cplusplus
}
#endif

#endif
