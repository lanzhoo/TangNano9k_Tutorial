/*+***********************************************************************************
 Filename: Blink02.ino
 Description: led and uart_tx demo, co-operated with Tangnano9k fpga based mcu project. 

 Modification:
   2022.11.23 Creation   H.Zheng

Copyright (C) 2022  Zheng Hui (hzheng@gzhu.edu.cn)

License: MulanPSL-2.0

***********************************************************************************-*/

#define GPIO_ODR      *((volatile unsigned int *)0x00000000)
#define UART0_TX_Send *((volatile unsigned int *)0x00000010)

/**
 * set led value as vData[5:0]
 */
//void modSetLed(volatile unsigned int vData) __attribute__((noinline));
void modSetLed(volatile unsigned int vData);

/**
 * send vData[7:0] to uart
 */
//void modUartSend(volatile unsigned int vData) __attribute__((noinline));
void modUartSend(unsigned int vData);

unsigned int gCount;

void setup() {
  // put your setup code here, to run once:
  gCount = 0;
}

void loop() {
  // put your main code here, to run repeatedly: 
  unsigned int i;

//  GPIO_ODR = gCount;
  modSetLed(gCount);
  modUartSend(gCount);

  gCount++;
  
  for (i=0;i<10;i++) {
    asm volatile(
      "nop;"
    );
  }
}

/**
 * set led value as vData[5:0]
 */
void modSetLed(volatile unsigned int vData) {
  GPIO_ODR = vData;
}

/**
 * send vData[7:0] to uart
 */
 void modUartSend(unsigned int vData) {
  UART0_TX_Send = vData;
 }
