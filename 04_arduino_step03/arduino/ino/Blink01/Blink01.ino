#define GPIO_ODR      *((volatile unsigned int *)0x00000000)
#define UART0_TX_Send *((volatile unsigned int *)0x00000010)

unsigned int gCount;

void setup() {
  // put your setup code here, to run once:
  gCount = 0;
}

void loop() {
  // put your main code here, to run repeatedly: 
  unsigned int i;

  GPIO_ODR = gCount;
  UART0_TX_Send = gCount;
  
  gCount++;
  
  for (i=0;i<10;i++) {
    asm volatile(
      "nop;"
    );
  }
}
