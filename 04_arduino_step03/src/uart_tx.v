/*
 *  PicoSoC - A simple example SoC using PicoRV32
 *
 *  Copyright (C) 2017  Clifford Wolf <clifford@clifford.at>
 *
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

/*+***********************************************************************************
 Filename: uart_tx.v
 Description: a simple uart tx implementation simplified from simpleuart.v of PicoSoC project.
    Baudrate is clk/BAUDRATE_DIVIDER. No parity.

 Modification:
   2022.10.05 copy and simplify   H.Zheng
***********************************************************************************-*/

module uart_tx #(parameter BAUDRATE_DIVIDER=234) (
    input wire clk,
    input wire reset_n,
    input wire tx_en,
    input wire [7:0] tx_data,
    output wire txd
);
    localparam FRAME_LEN = 10;

    reg [FRAME_LEN-1:0] send_pattern;
    reg [3:0] send_bitcnt;
    reg [31:0] send_divcnt;

    assign txd = send_pattern[0];

    always @(posedge clk) begin
        send_divcnt <= send_divcnt + 1;
        if (!reset_n) begin
            send_pattern <= ~0;
            send_bitcnt <= 0;
            send_divcnt <= 0;
        end else begin
            if (tx_en && !send_bitcnt) begin
                send_pattern <= {1'b1, tx_data[7:0], 1'b0};
                send_bitcnt <= FRAME_LEN;
                send_divcnt <= 0;
            end 
            else if ((send_divcnt >= BAUDRATE_DIVIDER) && send_bitcnt) begin
                send_pattern <= {1'b1, send_pattern[9:1]};
                send_bitcnt <= send_bitcnt - 1'b1;
                send_divcnt <= 0;
            end
        end
    end
endmodule