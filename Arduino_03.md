# 基于Aruduino IDE的设计（3）：for (bnez)和return (ret)等

上节介绍的Arduino IDE开发环境下的c语言例程，虽然可以编译下载，但是因为一些指令还没有实现，程序还没有跑起来。

本节首先补充指令，并提高core_clk频率，让程序跑起来。



## FPGA硬件设计

复制工程04_arduino_step01到04_arduino_step02，修改工程文件名字。

（似乎需要重新设置System Verilog2017选项）

### 增加指令实现

查看上节提到的程序反汇编源代码：
```
80002000 <_start>:
80002000:	00001137          	lui	sp,0x1
80002004:	0240006f          	j	80002028 <main>

80002008 <_fini>:
80002008:	0000006f          	j	80002008 <_fini>

8000200c <_Z5setupv>:
8000200c:	00008067          	ret

80002010 <_Z4loopv>:
80002010:	00180813          	addi	a6,a6,1
80002014:	00a00793          	li	a5,10
80002018:	00000013          	nop
8000201c:	fff78793          	addi	a5,a5,-1
80002020:	fe079ce3          	bnez	a5,80002018 <_Z4loopv+0x8>
80002024:	00008067          	ret

80002028 <main>:
80002028:	ff010113          	addi	sp,sp,-16 # ff0 <__global_pointer$+0x3ec>
8000202c:	00112623          	sw	ra,12(sp)
80002030:	fddff0ef          	jal	ra,8000200c <_Z5setupv>
80002034:	fddff0ef          	jal	ra,80002010 <_Z4loopv>
80002038:	ffdff06f          	j	80002034 <main+0xc>
```

需要增加实现的指令包括：
- lui：装立即数
- ret：函数返回
- li：装立即数
- bnez：条件跳转
- jal：jump and link （之前的link没实现，只实现了jump）


#### lui的实现

RISC-V规范2.4节：

“LUI (load upper immediate) is used to build 32-bit constants and uses the U-type format. LUI
places the U-immediate value in the top 20 bits of the destination register rd, filling in the lowest 12 bits with zeros.”

从指令中取出立即数（高20位，低12位补零：{inst[31:12],12'b0}），在回写阶段送rd。

从规范表19.2，LUI的opcode为7'b0110111。

实现代码：
- decode部分：
```
    wire inst_LUI = (opcode == 7'b0110111);
```
- 取立即数部分：
```
    wire [31:0] i_imm_U = {instruction[31:12], 12'b0};
```
- 回写部分：
```
//    assign rf_rd_data = (inst_ADI) ? adi_result :
//                          (opcode_LOAD) ? load_result : 32'b0;
//    assign rf_rdwen = inst_ADI | opcode_LOAD;

    assign rf_rd_data = (inst_ADI) ? adi_result :
                          (opcode_LOAD) ? load_result : 
                             (inst_LUI) ? i_imm_U : 32'b0;
    assign rf_rdwen = inst_ADI | opcode_LOAD | inst_LUI;
```


#### li的实现

分析如下指令：
```
80002014:	00a00793          	li	a5,10
```
其opcode是7'b0010011，实际上是一条ADDI rd, x0, imm指令，所以li是伪指令，不用额外实现。


#### jal的实现

RISC-V规范2.5节：

“The jump and link (JAL) instruction uses the J-type format, where the J-immediate encodes a signed offset in multiples of 2 bytes. The offset is sign-extended and added to the pc to form the jump target address. Jumps can therefore target a ±1 MiB range. JAL stores the address of the instruction following the jump (pc+4) into register rd. The standard software calling convention uses x1 as the return address register and x5 as an alternate link register.”

之前只实现了jump，没实现link：

“stores the address of the instruction following the jump (pc+4) into register rd.”

实现代码：
- 回写部分：
```
    wire [31:0] pc_plus_four = program_counter + 4;
    assign rf_rd_data = (inst_ADI) ? adi_result :
                          (opcode_LOAD) ? load_result : 
                             (inst_JAL &(~inst_J)) ? pc_plus_four :
                             (inst_LUI) ? i_imm_U : 32'b0;
    assign rf_rdwen = inst_ADI | opcode_LOAD | inst_LUI | (inst_JAL &(~inst_J));

```

#### ret的实现（JALR）

分析如下指令：
```
8000200c:	00008067          	ret
```
其opcode是7'b1100111，是一条JALR指令。

RISC-V规范2.5节：

“The indirect jump instruction JALR (jump and link register) uses the I-type encoding. The target address is obtained by adding the 12-bit signed I-immediate to the register rs1, then setting the least-significant bit of the result to zero. The address of the instruction following the jump (pc+4) is written to register rd. Register x0 can be used as the destination if the result is not required.”

JALR与JAL的区别是跳转地址由rs1+imm得到。

实现代码：
- decode部分：
```
    wire inst_JALR = (opcode == 7'b1100111);
```
- 取立即数部分，JALR的立即数采用I Type格式，原来已经实现：
```
    wire [31:0] i_imm_I = {{21{instruction[31]}},instruction[30:20]};
```
- 跳转部分：
```
    wire [31:0] rs1_plus_imm_i = rf_rs1data + i_imm_I;

    assign pc_next = (~reset_n_sync) ? 32'b0 :
                        inst_JALR ? rs1_plus_imm_i :
                          inst_JAL ? program_counter + i_imm_J : pc_plus_four;  
//    assign pc_next = (~reset_n_sync) ? 32'b0 :
//                          inst_JAL ? program_counter + i_imm_J : program_counter + 4;  
```
- 回写部分：
```
    wire [31:0] pc_plus_four = program_counter + 4;
    assign rf_rd_data = (inst_ADI) ? adi_result :
                          (opcode_LOAD) ? load_result : 
                             ((inst_JAL | inst_JALR) &(i_rd_idx != 5'b0)) ? pc_plus_four :
                             (inst_LUI) ? i_imm_U : 32'b0;
    assign rf_rdwen = inst_ADI | opcode_LOAD | inst_LUI | ((inst_JAL | inst_JALR) &(i_rd_idx != 5'b0));

```

#### bnez的实现（BNE）

bnez是branch if not equal zero，属于条件跳转指令组（BRANCH）中的一条。

RISC-V规范2.5节的“Conditional Branches”部分：

“All branch instructions use the B-type instruction format. The 12-bit B-immediate encodes signed offsets in multiples of 2, and is added to the current pc to give the target address. The conditional branch range is ±4 KiB.”

“Branch instructions compare two registers. BEQ and BNE take the branch if registers rs1 and rs2 are equal or unequal respectively. BLT and BLTU take the branch if rs1 is less than rs2, using signed and unsigned comparison respectively. BGE and BGEU take the branch if rs1 is greater than or equal to rs2, using signed and unsigned comparison respectively. Note, BGT, BGTU, BLE, and BLEU can be synthesized by reversing the operands to BLT, BLTU, BGE, and BGEU, respectively.”

我们先实现BNE指令。

实现代码：
- decode部分：
```
    wire opcode_BRANCH = (opcode == 7'b1100011);
    
    wire inst_BNE = opcode_BRANCH & (funct3 == 3'b001);
    
```
- 取立即数部分，BRANCH的立即数采用B Type格式：
```
    wire [31:0] i_imm_B = {{20{instruction[31]}},instruction[7],instruction[30:25],instruction[11:8],1'b0};
```
- 跳转部分：
```
    wire branch_in_effect = (inst_BNE & (rf_rs1data != rf_rs2data));

    assign pc_next = (~reset_n_sync) ? 32'b0 :
                        branch_in_effect ? program_counter + i_imm_B :
                        inst_JALR ? rs1_plus_imm_i :
                          inst_JAL ? program_counter + i_imm_J : pc_plus_four;  
```



#### 其它

观察x16的值：

core.v：
```
    assign monitor_port = {ibus_addr[1:0], x16_value[3:0]};
```

加快core的速度：
    
top.v：
```
//    wire core_clk = counter[24];
    wire core_clk = counter[20];
```


## 软件部分

没有变化。

```
void setup() {
  // put your setup code here, to run once:
}

void loop() {
  // put your main code here, to run repeatedly: 
  unsigned int i;
  asm volatile(
    "addi x16,x16,1;"
  );
  
  for (i=0;i<10;i++) {
    asm volatile(
      "nop;"
    );
  }
}
```

## 观察效果

烧写。

Arduino下载（上传）。

可以看到led[3:0]（x16的值）逐步加一。（对应语句：“addi x16,x16,1;”）

```
    assign monitor_port = {ibus_addr[1:0], x16_value[3:0]};
```

