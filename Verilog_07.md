# Verilog入门（7）:generate和for

本节通过例子介绍自动生成（复制）类似代码段的方法。

在设计中，我们可能会遇到模块中包含多个类似的电路块的情形。我们可以逐个把它们写下来，也可以通过Verilog提供的generate结构来自动生成。

涉及的关键字包括：generate、for、genvar等。

本节例子以计数器/2分频器为例，前面的例子用加法器实现计数/分频，有些浪费资源，可以用多个D触发器串联，每个D触发器的Q_bar接D、Q接下一级D触发器的时钟的方式来实现。

#### 代码

将前文的02_verilog_step03例子工程复制到新目录，改目录名，改工程文件名。

编辑top.v，输入以下代码：
```
module top
(
    input wire [1:0] button,
    input wire sys_clk,
    output wire [5:0] led
);


    reg [29:0] counter;   
    wire reset_n = button[1];

    //the first D flip-flop
    always@(posedge sys_clk or negedge reset_n) begin
        if (~reset_n) begin
            counter[0]<=0;
        end
        else begin
            counter[0] <= ~counter[0];
        end
    end
    //the other D flip-flops
    genvar i;
    generate
        for (i=1; i<30; i=i+1) begin : counter_regs
            always@(negedge counter[i-1] or negedge reset_n) begin
                if (~reset_n) begin
                    counter[i]<=0;
                end
                else begin
                    counter[i] <= ~counter[i];
                end
            end
        end
    endgenerate

    //
    assign led = ~counter[29:24];

endmodule

```

#### generate结构

generate结构包括3部分：
- genvar声明for语句中使用的变量；
- generate、endgenerate指示generate结构；
- for语句给出循环次数，for结构的begin与end之间的内容是要复制的代码；

for语句的形式与c语言很像，就不解释了。

for语句的begin后面加的冒号和“counter_regs”是给这个结构一个标识（名字），否则会出一个warning：
```
WARN  (EX3788) : Block identifier is required on this block
```

#### 对for语句的理解

Verilog是电路描述语言，不是计算机编程语言，这里的for是写给综合器预处理时看的，不是综合时看的。

它的作用是复制代码，比如上面的for等同于输入以下的代码：
```
            always@(negedge counter[0] or negedge reset_n) begin
                if (~reset_n) begin
                    counter[1]<=0;
                end
                else begin
                    counter[1] <= ~counter[1];
                end
            end
            always@(negedge counter[1] or negedge reset_n) begin
                if (~reset_n) begin
                    counter[2]<=0;
                end
                else begin
                    counter[2] <= ~counter[2];
                end
            end
......
            always@(negedge counter[28] or negedge reset_n) begin
                if (~reset_n) begin
                    counter[29]<=0;
                end
                else begin
                    counter[29] <= ~counter[29];
                end
            end
```

可见利用generate和for可以减少重复劳动。




