/*+***********************************************************************************
 Filename: 03_tinycore_step01\src\core.v
 Description: demo the construction of a simple tiny-core.
              step01: a core with only a program counter.

 Modification:
   2022.10.12 Creation   H.Zheng

Copyright (C) 2022  Zheng Hui (hzheng@gzhu.edu.cn)

License: MulanPSL-2.0

***********************************************************************************-*/

module core (
    input wire core_clk,
    input wire reset_n,
    output wire [31:0] ibus_addr,
    output wire ibus_re,  //read enable
    input wire [31:0] instruction,
    output wire [31:0] monitor_port
);
    //PC
    reg [31:0] program_counter;

    always @(posedge core_clk or negedge reset_n) begin
        if (~reset_n) begin
            program_counter <= 0;
        end
        else begin
            program_counter <= program_counter + 1'b1;
        end
    end

    assign ibus_addr = program_counter;
    assign ibus_re = 1'b1;

    //
    assign monitor_port = instruction;



endmodule