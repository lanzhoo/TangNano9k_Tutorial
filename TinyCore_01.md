# 简单CPU原型设计（1）:微机原理回顾；厂家IP模块引用

本节介绍一个非常简单的RISC-V CPU Core的设计代码。可供学习者加深理解CPU/Core的循环“取指令、解释指令、执行指令”的工作过程。

本节首先简单介绍CPU/Core的工作原理，然后介绍在云源软件中如何引用厂家IP模块，以PLL和ROM为例。


### 微机原理回顾

#### 芯片内部结构

单芯片微机的芯片内部结构，主要可以划分为四类：
- CPU/Core；
- 存储器（Memory）；
- 总线（Bus）；
- 外设（Peripheral）；

#### CPU/Core的基本工作原理

CPU/Core的工作原理，基本的就两方面：
- 不断地“取指令、解释指令、执行指令”；
- 中断：当输入中断信号有效时，CPU/Core中断当前指令的执行，跳转到特定入口地址执行中断服务程序，执行完毕返回断点继续执行；

#### CPU/Core的内部结构

CPU/Core的内部结构，基本包括：
- 控制器（控制电路）；
- 通用寄存器组（工作寄存器组）；
- 运算器（ALU）；

#### CPU/Core怎么取指令

- 指令是什么？
指令是特定格式的二进制序列。CPU/Core能够识别并执行相应动作。

- ISA是什么？
指令的集合称为指令集（Instruction Set），描述指令集的格式以及CPU/Core应该执行什么动作的规范称为“指令集架构”（ISA：Instruction Set Architechture）。

各种各样的CPU/Core按照其遵循的指令集架构进行分类，常见的ISA包括：
x86/amd64；ARM Cortex-M、ARM Cortex-A；
51；avr；RISC-V；
PowerPC；Xtensa；MIPS；

- 指令放在哪里？
指令放在存储器里。有的芯片设计中，保存指令的存储器模块和保存数据的存储器模块分开，称为指令存储器。

- CPU/Core怎样取指令？
CPU/Core通过总线访问存储器，对存储器中的内容进行读写。

存储器的接口（存储器总线）分为控制总线（选中、读/写）、地址总线和数据总线。

CPU/Core中有一个寄存器（通常称为Program Counter，PC）负责给出要读取的指令的地址，送到地址总线上，由控制电路产生控制信号送到控制总线上；然后存储器将指令送到数据总线上，CPU/Core从数据总线读取指令，进行后续的译码工作。


### 引用厂家IP模块

#### 例子
本节通过一个例子演示取指令的过程。

该例子的效果是通过led显示存储器中存储的指令的若干比特。

例子中使用一个计数器模拟PC寄存器，给出的计数值作为地址接存储器，存储器提供的数据接LED。

参考02_verilog_step03创建（复制）新工程（主要是用到了led和晶振时钟）。

#### PLL
板上晶振的频率往往比较低，不满足应用的要求。这时我们通过锁相环（PLL）来产生系统时钟。

FPGA芯片厂家一般都会提供若干设计好的模块给开发者选用，这些模块称为IP模块（Intellectual Property）。

打开工程，在云源软件菜单中点击“Tools/IP Core Generator”，选择Clock下面的rPLL：
![PLL](images/tinycore01_01.png)

双击，弹出配置窗口。拉动滚动条，修改输入频率（CLKIN）为27MHz：
![PLL](images/tinycore01_02.png)

拉动滚动条，修改输出频率（CLKOUT）为180MHz。点击CLKIN框下面的Calculate，弹出提示说没问题，查看CLKOUT框下的actual frequency，表示27MHz乘一个整数除一个整数能得到的最接近目标值的频率，如果不一致可以修改CLKOUT框中的Tolerance。：
![输入图片说明](images/tinycore01_03.png)



点“OK”，弹出窗口问是否加入工程，点“OK”。

在Design窗口会看到工程目录中自动加入了gowin_rpll.v。而在代码窗口会显示gowin_rpll_tmp.v文件的内容，该文件的内容是演示怎样引用Gowin_rPLL模块，把它的代码复制到自己代码中修改就好。
![输入图片说明](images/tinycore01_04.png)

将引用rpll的代码复制到top.v中，修改如下：

```
module top
(
    input wire [1:0] button,
    input wire sys_clk,
    output wire [5:0] led
);

    wire clk_200MHz;
    Gowin_rPLL m_pll(
        .clkout(clk_200MHz), //output clkout
        .clkin(sys_clk) //input clkin
    );


endmodule
```
这样，输入的27MHz时钟通过PLL实例产生了200MHz的时钟，等会用于RAM中。

#### PC寄存器

设计一个计数器模仿PC寄存器，提供地址给存储器。

```
    wire reset_n = button[1];

    //core clk
    reg [24:0] counter;

    always @(posedge sys_clk or negedge reset_n) begin
        if (~reset_n) begin
            counter <= 0;
        end
        else begin
            counter <= counter + 1'b1;
        end
    end

    wire core_clk = counter[24];

    //PC
    reg [31:0] program_counter;

    always @(posedge core_clk or negedge reset_n) begin
        if (~reset_n) begin
            program_counter <= 0;
        end
        else begin
            program_counter <= program_counter + 1'b1;
        end
    end
```
这里先将27MHz的时钟分频到1Hz左右，然后驱动program_counter计数。


#### ROM实例

先在src目录下创建文件rom_data.mi，用文本编辑器编辑，输入：
```
#File_format=Hex
#Address_depth=8
#Data_width=32
00000001
00000002
00000004
00000008
00000010
00000020
0000003f
00000000
```

在云源软件菜单中点击“Tools/IP Core Generator”，选择Memory/Block Memory下面的pROM：
![输入图片说明](images/tinycore01_05.png)

双击pROM，在弹出的界面上修改depth为8，width为32。填充数据文件选择rom_data.mi：
![输入图片说明](images/tinycore01_06.png)

OK后选择将文件加入工程。

将引用pROM的代码复制到top.v中，修改如下：

```
    wire [31:0] instruction;
    Gowin_pROM I_ROM(
        .dout(instruction), //output [31:0] dout
        .clk(clk_200MHz), //input clk
        .oce(1'b1), //input oce
        .ce(1'b1), //input ce
        .reset(~reset_n), //input reset
        .ad(program_counter[2:0]) //input [2:0] ad
    );
```

#### 输出

将ROM输出的数据接led显示。

```
    assign led = ~instruction[5:0];
```

烧写，观察led显示值与rom_data.mi中数据的关系。

注意：如果修改了rom_data.mi文件，需要重新生成gowin_prom.v，或者直接修改gowin_prom.v中的初始数据：
```
defparam prom_inst_0.INIT_RAM_00 = 256'h000000000000003F000000200000001000000008000000040000000200000001;
```


#### 小结

这是CPU/Core取指令过程的简单演示，包含了芯片的时钟电路、Core里面的Program Counter、总线和存储器。


#### 层次化代码

现在的core代码也在top.v中，没有很好地体现verilog的层次化结构。

可以在工程中新增core.v，输入代码：
```
module core (
    input wire core_clk,
    input wire reset_n,
    output wire [31:0] ibus_addr,
    output wire ibus_re,  //read enable
    input wire [31:0] instruction,
    output wire [31:0] monitor_port
);
    //PC
    reg [31:0] program_counter;

    always @(posedge core_clk or negedge reset_n) begin
        if (~reset_n) begin
            program_counter <= 0;
        end
        else begin
            program_counter <= program_counter + 1'b1;
        end
    end

    assign ibus_addr = program_counter;
    assign ibus_re = 1'b1;

    //
    assign monitor_port = instruction;

endmodule
```

然后修改top.v中代码，注释掉PC和ROM和输出的代码，新增：
```
    //core
    wire core_clk = counter[24];
    wire [31:0] ibus_addr;
    wire ibus_re;
    wire [31:0] instruction;
    wire [31:0] monitor_port;

    core m_core (
        .core_clk(core_clk),
        .reset_n(reset_n),
        .ibus_addr(ibus_addr),
        .ibus_re(ibus_re),
        .instruction(instruction),
        .monitor_port(monitor_port)
    );

    //IROM
    //address and control input from core
    //data output to core

    Gowin_pROM I_ROM(
        .dout(instruction), //output [31:0] dout
        .clk(clk_200MHz), //input clk
        .oce(ibus_re), //input oce
        .ce(1'b1), //input ce
        .reset(~reset_n), //input reset
        .ad(ibus_addr[2:0]) //input [2:0] ad
    );

    //display
    assign led = ~monitor_port[5:0];
```

分析一下模块之间的连接关系，对比一下两种风格的代码。
