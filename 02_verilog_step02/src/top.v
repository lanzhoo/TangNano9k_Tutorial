module top
(
    input wire [1:0] button,
    output wire [5:0] led
);
//unmask to select the function

//+ and -
/*
    assign led[1:0] = ~button;
    assign led[3:2] = ~(button+2'b01);
    assign led[5:4] = ~(button-2'b01);
*/

//MUX and shift
// /*
    assign led = (~button[1]) ? (~(6'h0c<<2)) :
                     (~button[0]) ? (~(6'h0c>>2)) : (~6'h0c);
// */
endmodule