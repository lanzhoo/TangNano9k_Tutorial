/*
 *  PicoSoC - A simple example SoC using PicoRV32
 *
 *  Copyright (C) 2017  Clifford Wolf <clifford@clifford.at>
 *
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

/*+***********************************************************************************
 Filename: uart_rx.v
 Description: a simple uart rx implementation simplified from simpleuart.v of PicoSoC project.
    Baudrate is clk/BAUDRATE_DIVIDER. No parity.

 Modification:
   2022.10.05 copy and simplify   H.Zheng
***********************************************************************************-*/

module uart_rx #(parameter BAUDRATE_DIVIDER=234) (
    input wire clk,
    input wire reset_n,
    input wire rxd,
    output wire [7:0] rx_data,
    output wire rx_flag
);

    reg [31:0] recv_divcnt;
	reg [7:0] recv_pattern;
	reg [7:0] recv_buf_data;
	reg recv_buf_valid;

    //FSM section 1
    localparam S_IDLE = 0, S_RX_START = 1, S_RX_DATA=2, S_RX_STOP_BIT=10;
    reg [3:0] current_state = 0, next_state;    

    always @(posedge clk or negedge reset_n) begin
        if (!reset_n) begin
            current_state <= S_IDLE;
        end else begin
            current_state <= next_state;
        end
    end

    //FSM section 2
    always @(*) begin
        next_state = 0;

        case (current_state) 
            S_IDLE: begin
                if (!rxd) begin  
                    next_state <= S_RX_START;
                end else begin
                    next_state <= S_IDLE;                        
                end
            end
            S_RX_START : begin
                if (2*recv_divcnt > BAUDRATE_DIVIDER) begin
                    next_state <= S_RX_DATA;
                end else begin
                    next_state <= S_RX_START;
                end
            end
            S_RX_STOP_BIT : begin
                if (recv_divcnt > BAUDRATE_DIVIDER) begin
                    next_state <= S_IDLE;
                end else begin
                    next_state <= S_RX_STOP_BIT;
                end
            end
            default: begin
                if (recv_divcnt > BAUDRATE_DIVIDER) begin
                    next_state <= current_state + 1'b1;
                end else begin
                    next_state <= current_state;
                end
            end
        endcase
    end

    //FSM section 3
	always @(posedge clk) begin
		if (!reset_n) begin
			recv_divcnt <= 0;
			recv_pattern <= 0;
			recv_buf_data <= 0;
			recv_buf_valid <= 0;
		end else begin
			recv_divcnt <= recv_divcnt + 1;
			recv_buf_valid <= 0;
            case (current_state) 
                S_IDLE: begin
                    recv_divcnt <= 0;
                end
                S_RX_START : begin
                    if (2*recv_divcnt > BAUDRATE_DIVIDER) begin
                        recv_divcnt <= 0;
                    end
                end
                S_RX_STOP_BIT : begin
                    if (recv_divcnt > BAUDRATE_DIVIDER) begin
                        recv_buf_data <= recv_pattern;
                        recv_buf_valid <= 1;
                    end
                end
                default: begin
                    if (recv_divcnt > BAUDRATE_DIVIDER) begin
                        recv_pattern <= {rxd, recv_pattern[7:1]};
                        recv_divcnt <= 0;
                    end
                end
            endcase
        end
    end

    //output
    assign rx_data = recv_buf_data;
    assign rx_flag = recv_buf_valid;

endmodule

