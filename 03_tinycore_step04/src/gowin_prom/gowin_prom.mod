-series GW1NR
-device GW1NR-9C
-package QFN88P
-part_number GW1NR-LV9QN88PC6/I5


-mod_name Gowin_pROM
-file_name gowin_prom
-path C:/hzheng/TangNano9k/gitee_project/03_tinycore_step01/src/gowin_prom/
-type RAM_ROM
-file_type vlg
-pROM true
-depth 8
-width 32
-read_mode bypass
-reset_mode sync
-init_file C:\hzheng\TangNano9k\gitee_project\03_tinycore_step01\src\rom_data.mi