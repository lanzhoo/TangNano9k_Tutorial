/*+***********************************************************************************
 Filename: A01_tinyriscv_mcu01\src\peripheral.v
 Description: a module to hold all kinds of peripherals.

 Modification:
   2022.11.07 Creation   H.Zheng (03_tinycore_step04\src\peripheral.v)
   2024.01.07 change addr to 12 bit width
   2024.02.08 add timer, add int_flag output port, clk_1MHz input port
              change peripheral unit address space to 16 
   2024.05.23 change button port to 3 bits for TangPrimer20kDock
              timer module add ce_i port
   2024.05.24 add intctrl module. change int_flag to 1 bit.
              add uart0_rx_valid_int.

Copyright (C) 2022  Zheng Hui (hzheng@gzhu.edu.cn)

License: MulanPSL-2.0

***********************************************************************************-*/

module peripheral (
    //from top
    input wire clk,
    input wire clk_1MHz,
    input wire reset_n,
    input wire [2:0] button,
    output wire [5:0] led,
    input wire rxd,
    output wire txd,
    //from core
    input wire ce, //chip enable
    input wire wre, //write enable
    input wire [11:0] addr, //address bus
    input wire [31:0] data_in,
    output wire [31:0] data_out,
    output wire int_flag
);
    //gpio
    //
    wire gpio_ce = (ce & (addr[11:4] == 8'h0)) ? 1'b1 : 1'b0;

    wire [31:0] gpio_data_out;
    gpio m_gpio(
        .clk(clk),
        .reset_n(reset_n),
        .button(button),
        .led(led),
        .ce(gpio_ce),
        .wre(wre),
        .addr(addr[3:0]),
        .data_in(data_in),
        .data_out(gpio_data_out)

    );

    //uart
    //
    wire uart0_ce = (ce & (addr[11:4] == 8'h1)) ? 1'b1 : 1'b0;

    wire [31:0] uart0_data_out;
    wire uart0_rx_valid_int;

    peripheral_uart m_uart0(
        .clk(clk),
        .reset_n(reset_n),
        .rxd(rxd),
        .txd(txd),
        .ce(uart0_ce),
        .wre(wre),
        .addr(addr[3:0]),
        .data_in(data_in),
        .data_out(uart0_data_out),
        .int_rx_valid_o(uart0_rx_valid_int)
    );

    //timer
    wire timer0_ce = (ce & (addr[11:4] == 8'h2)) ? 1'b1 : 1'b0;
    wire [31:0] timer0_data_out;
    wire timer0_int;
    
    timer m_timer0(
        .clk(clk),
        .reset_n(reset_n),
        .data_i(data_in),
        .addr_i(addr[3:0]),
        .we_i(timer0_ce & wre),
        .ce_i(timer0_ce),
        .data_o(timer0_data_out),
        .int_sig_o(timer0_int)
    );

    /**
     * interrupt controller
     */
    //equal mem bus addr: 0x3fc0-0x3fff
    wire int_ctrl_ce = (ce & (addr[11:4] == 8'hff)) ? 1'b1 : 1'b0;
    wire [31:0] int_ctrl_data_out;
    wire [31:0] int_req_sig_i;

    assign int_req_sig_i ={30'b0, uart0_rx_valid_int, timer0_int};

    interrupt_controller m_intctrl(
        .clk(clk),
        .reset_n(reset_n),
        .data_i(data_in),
        .addr_i(addr[3:0]),
        .we_i(wre),
        .ce_i(int_ctrl_ce),
        .data_o(int_ctrl_data_out),
        .int_req_sig_i(int_req_sig_i),
        .int_sig_o(int_flag)
    );

    //data out mux
    assign data_out = (gpio_ce) ? gpio_data_out :
                      (uart0_ce) ? uart0_data_out :
                      (timer0_ce) ? timer0_data_out :
                      (int_ctrl_ce) ? int_ctrl_data_out : 32'bz;


    
endmodule
