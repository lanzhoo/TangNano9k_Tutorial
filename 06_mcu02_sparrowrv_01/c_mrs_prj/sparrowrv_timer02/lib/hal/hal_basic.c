/*+***********************************************************************************
 Filename: hal_basic.v
 Description:
   Basic HAL functions for my TangPrimer20kDock based mcu. (03_mcu_sparrowrvcore_02)

 Modification:
   2024.05.25 Creation    H.Zheng

Copyright (C) 2024  Zheng Hui (hzheng@gzhu.edu.cn)

License: MulanPSL-2.0

***********************************************************************************-*/

//for my peripheral
#define GPIO_ODR      *((volatile unsigned int *)0x10000000)
#define GPIO_IDR      *((volatile unsigned int *)0x10000004)
#define UART_TXD      *((volatile unsigned int *)0x10000040)
#define UART_RXD      *((volatile unsigned int *)0x10000040)
#define UART_RX_VALID   *((volatile unsigned int *)0x10000044)
#define UART_TX_BUSY   *((volatile unsigned int *)0x10000048)
#define TIMER_CTRL    *((volatile unsigned int *)0x10000080)
#define TIMER_COUNTER *((volatile unsigned int *)0x10000084)
#define TIMER_TOP     *((volatile unsigned int *)0x10000088)
#define TIMER_OVERFLOW_FLAG     *((volatile unsigned int *)0x1000008c)
#define PERI_EFFECT_INT_REQ     *((volatile unsigned int *)0x10003fc0)
#define PERI_INT_MASK     *((volatile unsigned int *)0x10003fc4)
#define PERI_PENDING_INT_REQ     *((volatile unsigned int *)0x10003fc8)

#define PERI_INT_MASK_VALUE_TIMER 0x01
#define PERI_INT_MASK_VALUE_UART  0x02

/**
 * GPIO
 */
void setLedValue(unsigned int vValue) {
  GPIO_ODR = vValue;
}

unsigned int getLedValue() {
  return GPIO_ODR;
}

int buttonPushed(){
    unsigned int i, button;
    i = GPIO_IDR&0x07;
    if (i== 0x07)
        return 0;
    else if ((i&0x01)== 0)
        button=1;
    else if ((i&0x02)== 0)
        button= 2;
    else if ((i&0x04)== 0)
        button= 3;
    else
        button = 0;
    //wait for button released
    while ((GPIO_IDR&0x07)!= 0x07) {
    }
    //
    return button;
}

/**
 * TIMER
 */
void setTimerTopValue(unsigned int vValue) {
  TIMER_TOP = vValue;
}

void startTimer() {
  TIMER_CTRL = 0x01;
}

/**
 * UART
 */
void _putchar(char character){
    while (UART_TX_BUSY){
    }
    UART_TXD = character;

}

void uartSend(unsigned int vData) {
  _putchar(vData);
}

//blocking function: return until receive byte
unsigned int uartReceive() {
    while (!UART_RX_VALID){
    }
    return UART_RXD;
}

/**
 * Interrupt
 */
void enableGlobalInt() {
  __asm__ __volatile__("csrsi  mstatus,0x08");
}

void enableTimerInt() {
  PERI_INT_MASK |= PERI_INT_MASK_VALUE_TIMER;
}

void enableUartRxvalidInt() {
  PERI_INT_MASK |= PERI_INT_MASK_VALUE_UART;
}


/**
 * external interrupt handler
 */

//default timer overflow handler
unsigned int gTimeoutFlag;

__attribute__((weak)) void handler_timer_overflow() {
    gTimeoutFlag = TIMER_OVERFLOW_FLAG;
}

//default uart rxvalid handler
unsigned int gUartRxData;
unsigned int gUartRxDataFlag;

__attribute__((weak)) void handler_uart_rxvalid() {
  gUartRxData = UART_RXD;
  gUartRxDataFlag = 1;
}

//external interrupt handler
void handler_interrupt_ex() {
  unsigned int request;
  request = PERI_EFFECT_INT_REQ;
  if (request & PERI_INT_MASK_VALUE_TIMER) {
    handler_timer_overflow();
  }
  if (request & PERI_INT_MASK_VALUE_UART) {
    handler_uart_rxvalid();
  }

}
