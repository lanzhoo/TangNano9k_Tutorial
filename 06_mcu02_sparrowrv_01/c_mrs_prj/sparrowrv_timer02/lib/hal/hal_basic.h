/*+***********************************************************************************
 Filename: hal_basic.h
 Description:
   Basic HAL functions for my TangPrimer20kDock based mcu. (03_mcu_sparrowrvcore_02)

 Modification:
   2024.05.25 Creation    H.Zheng

Copyright (C) 2024  Zheng Hui (hzheng@gzhu.edu.cn)

License: MulanPSL-2.0

***********************************************************************************-*/
#ifndef HAL_BASIC_H
#define HAL_BASIC_H

/**
 * GPIO
 */
void setLedValue(unsigned int vValue);

unsigned int getLedValue();

int buttonPushed();

/**
 * TIMER
 */
void setTimerTopValue(unsigned int vValue);

void startTimer();

/**
 * UART
 */
void _putchar(char character);

void uartSend(unsigned int vData);

//blocking function: return until receive byte
unsigned int uartReceive();

/**
 * Interrupt
 */
void enableGlobalInt();

void enableTimerInt();

void enableUartRxvalidInt();


/**
 * external interrupt handler
 */

//default timer overflow handler
extern unsigned int gTimeoutFlag;

//default uart rxvalid handler
extern unsigned int gUartRxData;
extern unsigned int gUartRxDataFlag;


#endif
