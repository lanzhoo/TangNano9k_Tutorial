#include "printf.h"
#include "hal_basic.h"

/*
//for my peripheral
#define GPIO_ODR      *((volatile unsigned int *)0x10000000)
#define UART_TXD      *((volatile unsigned int *)0x10000040)
#define UART_RXD      *((volatile unsigned int *)0x10000040)
#define UART_RX_VALID   *((volatile unsigned int *)0x10000044)
#define UART_TX_BUSY   *((volatile unsigned int *)0x10000048)
#define TIMER_CTRL    *((volatile unsigned int *)0x10000080)
#define TIMER_COUNTER *((volatile unsigned int *)0x10000084)
#define TIMER_TOP     *((volatile unsigned int *)0x10000088)
#define TIMER_OVERFLOW_FLAG     *((volatile unsigned int *)0x1000008c)
#define PERIPHERAL_INT_MASK     *((volatile unsigned int *)0x10003fc4)


//
//extern void trap_entry();
void _putchar(char character){
    while (UART_TX_BUSY){
    }
    UART_TXD = character;

}
//
int gTimeoutFlag;

//timer interrupt handler
void handler_interrupt_tcmp() {
    gTimeoutFlag = TIMER_OVERFLOW_FLAG;
}
//external interrupt handler
void handler_interrupt_ex() {
//    GPIO_ODR = 0x10 + TIMER_OVERFLOW_FLAG;
//    TIMER_CTRL = 0x01;
    gTimeoutFlag = TIMER_OVERFLOW_FLAG;
}
*/

int main(){
    printf("start...\n");

    setLedValue(1);

    gTimeoutFlag = 0;
    setTimerTopValue(13500000);
    enableTimerInt();
    enableGlobalInt();
    startTimer();

    while(1){
        if (gTimeoutFlag) {
            setLedValue(getLedValue()+1);
            printf("timeout...\n");
            gTimeoutFlag = 0;
        }
    }
}


