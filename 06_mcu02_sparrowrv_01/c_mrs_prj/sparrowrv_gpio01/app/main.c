/*+********************************************************
Filename: main.c
Description: simple led control example.
  Run on TangPrimer20kDock FPGA based self-defined MCU
  peripheral with SparrowRV core.

Modification:
2024.05.05 creation H.Zheng
********************************************************-*/


//peripheral memory map
#define GPIO_ODR      *((volatile unsigned int *)0x10000000)
#define UART_TXD      *((volatile unsigned int *)0x10000040)
#define UART_RXD      *((volatile unsigned int *)0x10000040)
#define UART_STATUS   *((volatile unsigned int *)0x10000044)
#define TIMER_CTRL    *((volatile unsigned int *)0x10000080)
#define TIMER_COUNTER *((volatile unsigned int *)0x10000084)
#define TIMER_TOP     *((volatile unsigned int *)0x10000088)
//

int gValue;
int main(){
    unsigned int i;
    gValue =0x12345678; //verify data ram storage
    GPIO_ODR = gValue;  //ensure gValue is not swept by optimization
    GPIO_ODR = 0x12;
    while (1){
        GPIO_ODR = GPIO_ODR + 1;
        //software delay
        for (i=1500000;i>0;i--){
        }
    }

}
