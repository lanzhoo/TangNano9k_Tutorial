#include "printf.h"
#include "hal_basic.h"


int main(){
    printf("start...\n");

    setLedValue(1);

    enableUartRxvalidInt();
    enableGlobalInt();

    while(1){

        if (gUartRxDataFlag) {
            setLedValue(gUartRxData);
            printf("rx: %x\n", gUartRxData);
            gUartRxDataFlag = 0;
        }

    }
}


