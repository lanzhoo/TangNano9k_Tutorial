/*+********************************************************
Filename: main.c
Description: simple uart example.
  Run on TangPrimer20kDock FPGA based self-defined MCU
  peripheral with SparrowRV core.

Modification:
2024.05.05 creation H.Zheng
********************************************************-*/


//peripheral memory map
#define GPIO_ODR      *((volatile unsigned int *)0x10000000)
#define UART_TXD      *((volatile unsigned int *)0x10000040)
#define UART_RXD      *((volatile unsigned int *)0x10000040)
#define UART_STATUS   *((volatile unsigned int *)0x10000044)
#define TIMER_CTRL    *((volatile unsigned int *)0x10000080)
#define TIMER_COUNTER *((volatile unsigned int *)0x10000084)
#define TIMER_TOP     *((volatile unsigned int *)0x10000088)
//

int gValue;
int main(){
    unsigned int i;
    gValue = 0x1;
    while (1){
        GPIO_ODR = gValue;
        UART_TXD = gValue;

        for (i=1500000;i>0;i--){
        }
        if ((UART_STATUS&0x01) ){
            UART_TXD = UART_RXD;
        }
        for (i=1500000;i>0;i--){
        }
        gValue += 1;
    }

}
