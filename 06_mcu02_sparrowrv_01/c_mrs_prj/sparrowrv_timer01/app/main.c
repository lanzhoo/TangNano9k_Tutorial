/*+********************************************************
Filename: main.c
Description: simple timeout example.
  Run on TangPrimer20kDock FPGA based self-defined MCU
  peripheral with SparrowRV core.

Modification:
2024.05.05 creation H.Zheng
********************************************************-*/


//peripheral memory map
#define GPIO_ODR      *((volatile unsigned int *)0x10000000)
#define UART_TXD      *((volatile unsigned int *)0x10000040)
#define UART_RXD      *((volatile unsigned int *)0x10000040)
#define UART_STATUS   *((volatile unsigned int *)0x10000044)
#define TIMER_CTRL    *((volatile unsigned int *)0x10000080)
#define TIMER_COUNTER *((volatile unsigned int *)0x10000084)
#define TIMER_TOP     *((volatile unsigned int *)0x10000088)
#define TIMER_OVERFLOW_FLAG     *((volatile unsigned int *)0x1000008c)

//

int main(){
    GPIO_ODR = 0x0;
    TIMER_TOP = 13500000;
    TIMER_CTRL = 0x01;
   while (1){
        GPIO_ODR = GPIO_ODR + 1;
        while (!TIMER_OVERFLOW_FLAG) {
            //wait for timeout
        }
    }

}
