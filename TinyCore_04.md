# 简单CPU原型设计（4）：简单GPIO

之前的LED亮灭不是通过代码实现控制的，只是将CORE内部的寄存器或变量引出用作指示，本节演示如何加入简单的外设模块，通过LOAD/STORE指令控制外设。


## 概念

### 软件如何控制硬件（芯片外围电路）？

软件指令通过读写外设开放给CPU/Core的I/O寄存器，从而控制外设的行为，从而控制引脚的输出电平或者读入引脚的输入电平，从而控制芯片的外围电路或者获取外围电路的信号。

Core怎样访问外设的I/O寄存器？

通过总线访问。




### 外设设计的大致思路

首先定义一个外设模块；然后设计它的I/O寄存器，并且使之能让Core通过总线访问。

然后定义一些端口和引脚相连；然后设计外设模块的内部逻辑，体现它的功能。

然后设计内部逻辑使I/O寄存器能够控制外设的行为；最后设计软件例子。



#### GPIO外设

基本所有的单片机/嵌入式芯片都有GPIO这种外设。

一般来说，为了让芯片有一定的通用性，GPIO模块的端口和引脚的连接关系是可配置的（引脚复用：某个引脚可以连接到GPIO模块，也可以连接到其它外设模块），引脚的输入输出属性也是可配置的。

GPIO外设最基本的I/O寄存器一般有三个：
- Direction：设置输入输出方向；
- Output Value：设置输出值；
- Input Value：读入输入值；

其它的可选寄存器包括：上拉下拉配置、位控制、电流强度等。

芯片手册中，引脚复用配置寄存器一般也包括在GPIO外设模块中介绍。


在FPGA中，因为可以根据外部电路实际连接状况进行电路设计，因此可以简化为两个I/O寄存器：
- Output Value：设置输出值；
- Input Value：读入输入值；




## 例程设计

### 硬件实现

复制工程03_tinycore_step03到03_tinycore_step04，修改工程文件名称。

#### 增加外设模块

为保持工程的层次化结构，新创建一个Peripheral模块，用于包含所有的外设。

peripheral.v:
```
module peripheral (
    input wire clk,
    input wire reset_n,
    input wire button,
    output wire [5:0] led
);



endmodule
```

Top模块包含Core、Rom、Ram和Peripheral模块。

将Top模块的led端口和按键端口接到Peripheral模块。


top.v:
```
    //peripherals
    peripheral m_peripheral(
        .clk(sys_clk),
        .reset_n(reset_n),
        .button(button[0]),
        .led(led)
    );
```

在core模块中，增加访问外设的总线。core.v：

```
module core (
...
    //peripheral
    output wire peripheral_ce, //chip enable
    output wire peripheral_wre, //write enable
    output wire [8:0] peripheral_addr, //address bus
    input wire [31:0] peripheral_data_in,
    output wire [31:0] peripheral_data_out,
    //
    output wire [31:0] monitor_port
);
```

在peripheral模块中，增加来自core的总线端口。peripheral.v：
```
module peripheral (
    //from top
    input wire clk,
    input wire reset_n,
    input wire button,
    output wire [5:0] led,
    //from core
    input wire ce, //chip enable
    input wire wre, //write enable
    input wire [8:0] addr, //address bus
    input wire [31:0] data_in,
    output wire [31:0] data_out
);
```

将Core模块的新增总线端口接到Peripheral模块。top.v：

```
    //
    wire peripheral_ce;
    wire peripheral_wre;
    wire [8:0] peripheral_addr;
    wire [31:0] peripheral_data_in;
    wire [31:0] peripheral_data_out;


    core m_core (
        .core_clk(core_clk),
        .reset_n(reset_n),
        .ibus_addr(ibus_addr),
        .ibus_re(ibus_re),
        .instruction(instruction),
        .ram_ce(ram_ce),
        .ram_wre(ram_wre),
        .ram_addr(ram_addr),
        .ram_data_in(ram_data_in),
        .ram_data_out(ram_data_out),
        //
        .peripheral_ce(peripheral_ce),
        .peripheral_wre(peripheral_wre),
        .peripheral_addr(peripheral_addr),
        .peripheral_data_in(peripheral_data_in),
        .peripheral_data_out(peripheral_data_out),
        //
        .monitor_port(monitor_port)
    );
```

```
    //peripherals
    peripheral m_peripheral(
        .clk(sys_clk),
        .reset_n(reset_n),
        .button(button[0]),
        .led(led),
        //
        .ce(peripheral_ce),
        .wre(peripheral_wre),
        .addr(peripheral_addr),
        .data_in(peripheral_data_out),
        .data_out(peripheral_data_in)
    );
```

注释掉原来的led输出：
```
    //display
//    assign led = ~monitor_port[5:0];

```


#### 增加GPIO模块

新建gpio模块，在Peripheral模块中包含gpio模块。

gpio.v：
```
module gpio (
    input wire clk,
    input wire reset_n,
    input wire button,
    output wire [5:0] led,
    input wire ce, //chip enable
    input wire wre, //write enable
    input wire [1:0] addr, //address bus
    input wire [31:0] data_in,
    output wire [31:0] data_out
);


endmodule

```

peripheral.v：
```
module peripheral (
    //from top
    input wire clk,
    input wire reset_n,
    input wire button,
    output wire [5:0] led,
    //from core
    input wire ce, //chip enable
    input wire wre, //write enable
    input wire [8:0] addr, //address bus
    input wire [31:0] data_in,
    output wire [31:0] data_out
);

    wire gpio_ce = (ce & (addr[8:2] == 7'b0)) ? 1'b1 : 1'b0;

    gpio m_gpio(
        .clk(clk),
        .reset_n(reset_n),
        .button(button),
        .led(led),
        .ce(gpio_ce),
        .wre(wre),
        .addr(addr[1:0]),
        .data_in(data_in),
        .data_out(data_in)

    );

endmodule
```

假设外设地址空间的0x000-0x003单元分配给GPIO，每个地址单元32位数据。

则gpio模块的ce信号为：
```
    wire gpio_ce = (ce & (addr[8:2] == 7'b0)) ? 1'b1 : 1'b0;
```





在gpio模块中，新增I/O寄存器：输出数据寄存器，Core可以读写。

```
    reg [31:0] data_out_reg;

    //write data_out register
    always @(posedge clk) begin
        if (~reset_n) begin
            data_out_reg <= 32'b0;
        end
        else if (ce & wre & (addr==2'b00)) begin
            data_out_reg <= data_in;
        end
    end    
```

在gpio模块中，新增I/O寄存器：输入数据寄存器，Core可以读。

```
    reg [31:0] data_in_reg;
    //latch input
    always @(posedge clk) begin
        if (~reset_n) begin
            data_in_reg <= 32'b0;
        end
        else begin
            data_in_reg <= {31'b0, button};
        end
    end    

```
其中，将用户按键引脚对应的端口接GPIO的输入数据寄存器的输入（bit0）。

（注：另一个按键作为软件reset，不接入GPIO模块）
```
            data_in_reg <= {31'b0, button};

```



根据ce信号、读信号和地址信号决定送上core总线的数据：（这里32'bz的z表示高阻）

```
    //whie core read

    assign data_out = ~((ce == 1'b1) & (wre == 1'b0)) ? 32'bz :   //if not read, return high Z
                        (addr==2'b00) ? data_out_reg :
                        (addr==2'b01) ? data_in_reg : 32'bz;


```
  

将LED引脚对应的端口接GPIO的输出数据寄存器的输出（bit[5:0]）。这里为了led方便显示数据，先取反再接led：
```
    //led
    assign led = ~data_out_reg[5:0];
```

#### 修改core，增加外设总线相关代码

这里，首先假设memory map的设计为：0x00000000 - 0x000003ff为数据ram地址，0x00000400 - 0x000007ff为外设地址。

则外设选通信号为：

```
    assign peripheral_ce = (loadstore_addr[31:11] == 20'b0) & (loadstore_addr[10] == 1'b1) & (opcode_LOAD | opcode_STORE);
```

修改ram_ce信号为：
```
    assign ram_ce = (loadstore_addr[31:10] == 22'b0) & (opcode_LOAD | opcode_STORE);
```

其它信号（wre、addr、data_out）与ram类似，因为都是通过LOAD/STORE指令访问。
```
    assign peripheral_wre = opcode_STORE & (~core_clk);

    assign peripheral_addr = {1'b0, loadstore_addr[9:2]}; //loadstore_addr is in bytes but peripheral_addr is in words

    //output data from rs2
    assign peripheral_data_out = rf_rs2data;
```

修改rd回写数据来源，增加来自外设的数据：

```
    wire [31:0] load_result = (ram_ce & opcode_LOAD) ? ram_data_in :
                                 (peripheral_ce & opcode_LOAD) ? peripheral_data_in : 32'b0;

    assign rf_rd_data = (inst_ADI) ? adi_result :
                          (opcode_LOAD) ? load_result : 32'b0;
    assign rf_rdwen = inst_ADI | opcode_LOAD;
```


### 软件实现

为了验证gpio模块的功能实现，软件程序流程改为：
- 循环：
- 从存储单元m0读数据到x15；
- x15+1送x16；
- 将x16的值保存到存储单元m0；
- 将x16的值保存到GPIO模块的数据输出I/O寄存器；



#### 程序代码

程序代码在rom_data.mi中，5条指令。须重新生成gowin_prom。

```
#File_format=Hex
#Address_depth=8
#Data_width=32
00002783
00178813
01002023
41002023
ff1ff06f
00000000
00000000
00000000
```

或者直接修改gowin_prom.v：
```
defparam prom_inst_0.INIT_RAM_00 = 256'h000000000000000000000000ff1ff06f41002023010020230017881300002783;
```

#### 指令解读
汇编程序为：
LOOP: LW x15, 0(x0)
      ADDI x16, x15, #1
      SW x16, 0(x0)
      SW x16, 1024(x0)      
      J LOOP

其中前3条与前一节的相同，第5条汇编语言与前一节第4条相同，但是机器码不一样。      
      
第1条指令：从RAM地址0处取值送x15。LW格式为<12b imm=0><5b rs1=0> func3=010 <5b rd=01111> opcode=0000011 

二进制值为：0b0000,0000,0000,0000,0010,0111,1000,0011。即0x00002783

对应汇编指令为：LW x15, 0(x0)

第2条指令：x15加1送x16。ADDI格式为<12b imm=1><5b rs1=01111> func3=000 <5b rd=10000> opcode=0010011

二进制值是：0b0000,0000,0001,0111,1000,1000,0001,0011。即0x00178813

对应汇编指令为：ADDI x16, x15, #1

第3条指令：将x16的值送RAM地址0单元。SW格式为<7b imm=0><5b rs2=10000><5b rs1=0> func3=010 <5b imm=0> opcode=0100011 

二进制值为：0b0000,0001,0000,0000,0010,0000,0010,0011。即0x01002023

对应汇编指令为：SW x16, 0(x0)

第4条指令：将x16的值送外设地址0单元即0x0400处。SW格式为<7b imm=0100000><5b rs2=10000><5b rs1=0> func3=010 <5b imm=0> opcode=0100011 

二进制值为：0b0100,0001,0000,0000,0010,0000,0010,0011。即0x41002023

对应汇编指令为：SW x16, 1024(x0)

第5条指令：跳转到4条指令前即16字节。JAL格式为<imm[20|10:1|11|19:12]><5b rd=0> opcode=1101111

-16对应1..11,0000。注意bit0不在指令里。所以<imm[20|10:1|11|19:12]>值为1|1111111000|1|11111111

二进制值为：0b1111,1111,0001,1111,1111,0000,0110,1111。即0xff1ff06f

对应汇编指令为：JAL x0, -16

大概等效c语言程序为：
```
#define MEM_UNIT_0 *((volatile unsigned int *)0x00000000)
#define GPIO_ODR *((volatile unsigned int *)0x00000400)

void main(void) {
  int i, j;
  while(1) {
    i = MEM_UNIT_0;
    j=i+1;
    MEM_UNIT_0 = j;
    GPIO_ODR = j;
  }
}
```

#### 运行效果


可以观察到led约每5秒加1。


