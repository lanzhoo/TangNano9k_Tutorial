# 完整MCU例子：基于tinyriscv core + JTAG + MounRiver_Studio

例程目录：[06_tinyriscv_01_mcu01](06_tinyriscv_01_mcu01)；其中c工程代码：06_tinyriscv_01_mcu01/c_mrs_prj


### 概述

本文介绍一个接近常见MCU开发方式的MCU实现方案。

其中c语言代码的编辑、编译、链接采用IDE工具MounRiver_Studio；烧写采用JTAG方式：命令行openocd + nanoDAPv2.3 JTAG模块。（该方案学习自“小麻雀处理器”https://gitee.com/xiaowuzxc/SparrowRV ）

芯片的内部结构：jtag代码（debug子目录和util子目录）、core（core子目录）来自“tinyriscv”仓库（https://gitee.com/liangkangnan/tinyriscv ）（简单修改），其它是本人代码。

### 环境搭建及程序运行

#### 硬件
首先需要购买一个额外的JTAG调试模块，我使用的是muselab-tech.com的nanoDAPv2.3模块。

硬件连接：
- jtag_TCK接25脚；
- jtag_TMS接26脚；
- jtag_TDI接27脚；
- jtag_TDO接28脚；
- jtag_GND接GND；

![JTAG模块连接照片](images/mcu01_01.png)

#### IDE软件
MounRiver Studio从官网下载安装（http://www.mounriver.com/ ）。

openOCD及配置文件来自“小麻雀处理器”仓库（https://gitee.com/xiaowuzxc/SparrowRV ），在本工程中也复制了（openocd.rar）。


#### 烧写FPGA MCU硬件

在Gowin云源软件下打开verilog工程，综合，下载。

#### 编译c例程

在MounRiver中打开c工程，编译链接。（注意工程目录不要带中文）

#### 烧写程序

通过MounRiver下载程序的方式我还没走通，下面是通过命令行窗口运行openocd的方式。（windows下）

Windows需启动telnet服务（具体方法自行搜索）。

Windows下打开命令行窗口的方式是：同时按下“视窗”按键和“R”按键，在弹出的窗口中输入“cmd”。

复制MounRiver编译生成的tinyriscv.bin到openocd及配置文件所在目录。

打开两个命令行窗口。

在cmd窗口1：
- 执行“cd <openocd及配置文件所在目录>”；
- 执行“openocd.exe -f tinyriscv.cfg”；

在cmd窗口2：
- 执行“telnet localhost 4444”；
- 等待连接成功；
- 执行“load_image tinyriscv.bin 0x0 bin 0x0 0x1000000”；
- 执行“verify_image tinyriscv.bin 0x0”；(可选)
- 执行“resume”；

![cmd窗口1截图](images/mcu01_02.png)

![cmd窗口2截图](images/mcu01_03.png)

#### Reset模块

按键S2是MCU的reset按键。（按键S1是JTAG的reset按键）

按动S2，可以看到LED显示约每秒加1。有时没有效果，多按几次reset。或者看看是否忘了在cmd窗口2执行resume。

#### 修改代码再次烧写

在MounRiver Studio修改代码、重新编译后：
- 复制tinyriscv.bin到openocd及配置文件所在目录；
- 在cmd窗口2：执行“halt”；
- 在cmd窗口2：执行“load_image tinyriscv.bin 0x0 bin 0x0 0x1000000”；
- 在cmd窗口2：执行“resume”；
- 按S2 Reset MCU；

在load_image命令行使用绝对路径可以省下复制bin文件的步骤，注意要将路径中的斜杠改为反斜杠。比如：
- 在cmd窗口2：执行“load_image C:/hzheng/TangNano9k/gitee_project/06_tinyriscv_01_mcu01/openocd/tinyriscv.bin 0x0 bin 0x0 0x1000000”；



### 实现简介

#### 芯片整体架构
使用两块双口RAM分别保存指令和数据。Core模块接双口RAM的B口，JTAG模块通过mux_2x3模块接双口RAM的A口。

mux_2x3模块是定制的总线转接模块，在JTAG需要时优先将从总线接JTAG总线。三个从总线接口分别接指令RAM的A口、外设、数据RAM的A口。

地址分配在mux_2x3模块内部实现：

- 指令RAM：0x0000_0000 - 0x0fff_ffff；
- 外设：0x1000_0000 - 0x1fff_ffff；
- 数据RAM：0x2000_0000 - 0x2fff_ffff；

JTAG模块可以通过mux_2x3模块读写外设中的I/O寄存器，有助于直接通过openOCD调试外设模块。

Core模块通过mux_2x3模块读写外设中的I/O寄存器。仅在访问外设地址范围时才激活mux_2x3模块的第2个主接口。

Core模块通过简单的指令读取接口从指令存储器读取指令，送指令译码模块。

Core模块通过无等待的简单系统总线访问数据存储器和外设（通过mux_2x3模块），数据多路选择在top中直接实现。

JTAG与Core之间的连接主要是一个halt端口。Reset、hold和寄存器读写接口未验证。（不知道怎样实现单步调试）


相对于原来的“tinyriscv”仓库工程（https://gitee.com/liangkangnan/tinyriscv ），主要的变动是更改了存储器的实现方式（ram和rom）、总线和外设，以及从“小麻雀处理器”仓库（https://gitee.com/xiaowuzxc/SparrowRV ）学习到的上位机开发方式（MounRiver + openOCD）。

Core中的一个小修改是注释了除法实现模块，以节省资源。

目前的资源占用为：(32kB ROM, 16kB RAM)
![资源占用](images/mcu01_04.png)


#### JTAG

JTAG模块直接使用“tinyriscv”仓库的代码。注意的是jtag_rst_n需要单独给它配一个按键，直接拉高不行；以及时钟使用1MHz。

上位机用的是“小麻雀处理器”仓库中的openOCD程序及配置文件。其配置文件内容：

```
adapter_khz     1000

reset_config srst_only
adapter_nsrst_assert_width 100
interface cmsis-dap
transport select jtag
#debug_level 3
set _CHIPNAME riscv
jtag newtap $_CHIPNAME cpu -irlen 5 -expected-id 0x1e200a6f
set _TARGETNAME $_CHIPNAME.cpu
target create $_TARGETNAME riscv -chain-position $_TARGETNAME
riscv set_reset_timeout_sec 1
init
halt
```
由于其中的“adapter_khz     1000”，因此需要产生一个1MHz的时钟作为JTAG模块的时钟。不确定用高点的行不行。试过27MHz不行。


#### MounRiver工程配置

链接文件link.lds做过修改。根据芯片配置设置指令ram（iram）、数据ram（sram）的大小以及起始地址。

```
_cpu_iram_size = 32K; /*iram指令存储器大小*/
_cpu_sram_size = 16K; /*sram数据存储器大小*/
_cpu_stack_size = 4K; /*堆栈区大小*/

_iram_base_addr = 0x00000000; /*iram存储器基地址*/
_sram_base_addr = 0x20000000; /*sram存储器基地址*/
```

工程参数：

设置risc-v architecture为RV32I：
![risc-v architecture](images/mcu01_05.png)

设置link script：
![设置link script](images/mcu01_06.png)


另，修改debug configuration为“-f "${eclipse_home}toolchain/OpenOCD/bin/tinyriscv.cfg"”并复制tinyriscv.cfg到对应目录，偶然能够通过MounRiver的debug下载代码，但是不能单步。


#### tinyriscv core

要注意的地方：
- 端口rst要接reset_n，低电平有效；
- 给core的时钟信号core_clk在reset时仍然要存在；
- 用core_clk的下降沿对RAM写入数据；（指令ram和数据ram模块的时钟信号设为~core_clk，原因见以下时序分析）

tinyriscv core的一些内部实现时序：
- 对于ex模块，当输入的指令数据变化时，mem_req、mem_we、mem_waddr_o、mem_raddr_o、mem_wdata_o随即发生变化。
- 对于SB、SH、SW指令，输出的地址rib_ex_addr_o为mem_waddr_o（不过mem_waddr_o和mem_raddr_o的值都一样）。
- 对于SB、SH指令，输出的值mem_wdata_o与mem_rdata_i有关。也就是说，给出内存单元地址，内存需要提供该单元的内容，然后运算后写回。
- 第1个时钟：更改pc值（clk的上升沿更改），送rom总线；
- 第2个时钟：在if_id锁存指令（clk的上升沿锁存），送id译码；
- 第3个时钟：指令在id_ex锁存送ex（clk的上升沿锁存），产生ram总线信号；（ram写入数据、提供数据）
- 第4个时钟：将ram数据回写寄存器；（clk的上升沿回写）

#### openOCD的一些操作

查看存储器内容：mdw <地址> <长度>

修改存储器内容：mww <地址> <值>


#### 外设：GPIO

输出寄存器地址0x10000000，接LED:

```
assign led = ~data_out_reg[5:0];
```

输入寄存器地址0x10000004，接按键S2（用作了jtag_reset）:

```
            data_in_reg <= {31'b0, button};
```

c测试代码：

```
#define GPIO_ODR      *((volatile unsigned int *)0x10000000)

int gValue;
int main(){
    unsigned int i;
    gValue = 1;

    while(1)    {
      GPIO_ODR = GPIO_ODR + 1;

      for (i=0;i<1500000;i++){
      }
    }
}
```



#### 外设：UART

发送寄存器地址和接收寄存器地址都是0x10000040。

状态寄存器地址0x10000044:
- bit0表示收到数据；
- bit1表示发送进行中；

```
    assign data_out = rx_data_en ? data_rx_reg :
                      status_rd_en ? {30'b0, status_tx_busy, status_rx_valid} : 32'b0;
```

uart收发具体实现代码改自PicoSoC的simpleuart.v。


c测试代码：

```
#define UART_TXD      *((volatile unsigned int *)0x10000040)
#define UART_RXD      *((volatile unsigned int *)0x10000040)
#define UART_STATUS   *((volatile unsigned int *)0x10000044)

int main(){
    while(1)    {
        if ((UART_STATUS&0x01) ){
            UART_TXD = UART_RXD;
        }
    }
}
```


#### 外设：定时器

控制寄存器地址0x10000080。
- bit0=1，启动定时器；
- bit1表示overflow；

计数器当前值寄存器地址0x10000084，可读可写。

计数器上限值寄存器（counter_top）地址0x10000088。overflow周期=(counter_top+1)*clk_period。

```
    always @(posedge clk) begin
        if (reset_n == 1'b0) begin
            counter <= 32'b0;
        end
        else if (we_i &&(addr_i == 4'h1)) begin //load counter initial value
            counter <= data_i;
        end 
        else begin
            if (counter >= counter_top) begin //overflow
                counter <= 32'b0;
            end
            else if (timer_ctrl[0]) begin  //tick counter
                counter <= counter + 1;
            end
        end
    end
```

需要在代码中手工清除overflow标志。

c测试代码：

```
#define GPIO_ODR      *((volatile unsigned int *)0x10000000)
#define TIMER_CTRL    *((volatile unsigned int *)0x10000080)
#define TIMER_COUNTER *((volatile unsigned int *)0x10000084)
#define TIMER_TOP     *((volatile unsigned int *)0x10000088)

int gValue;
int main(){
    unsigned int i;
    gValue = 1;
    GPIO_ODR = gValue;
    TIMER_TOP = 27000000;
    TIMER_CTRL = 0x01;
    while (1){
        GPIO_ODR = GPIO_ODR + 1;
        while (!(TIMER_CTRL&0x02)) {
            //wait for timeout
        }
        TIMER_CTRL = 0x01;

    }
}
```

### 存在问题

在调试定时器的中断应用代码时，发现tinyriscv core的clint.v代码有些问题：
- 在S_CSR_MSTATUS状态下关中断；
- 在S_CSR_MSTATUS_MRET状态下复制mPIE到mIE；
- 但是，没有发现中断时复制mIE到mPIE的代码。

也就是说，开全局中断后，能够中断一次，但是退出中断后的全局中断是关的。

尝试修改clint.v代码没成功。


后续先在TangPrimer20k上移植“小麻雀处理器”的core。因为目标是打算跑ohos liteos-m，本工程32k指令内存的容量不大够。
