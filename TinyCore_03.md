# 简单CPU原型设计（3）：数据存储器、LOAD/STORE

上节以加法指令和跳转指令为例演示了译码与执行过程及Core内部结构，本节加入数据LOAD/STORE的处理。


## 概念

### LOAD/STORE

精简指令集架构（RISC）的一个特点是运算类指令不能对数据存储器单元内的数据直接进行运算，只能对CORE内部的通用寄存器、特殊寄存器（PC、SP等）中的数据进行运算，结果也只能保存在通用寄存器、特殊寄存器中。为此设计了LOAD/STORE类别的指令负责在数据存储器和通用寄存器之间交换数据。

LOAD是将数据从数据存储器装载到通用寄存器，等待后续运算；STORE是将运算结果从通用寄存器保存到数据存储器。

### RISC-V RV32I LOAD/STORE

（以下文字基于2017年的版本进行解读。本系列帖子围绕RV32I进行实现和讲解。）

（RISC-V规范来源：https://riscv.org/technical/specifications/）

RV32I中的通用寄存器是32位的。RV32I定义了LOAD类型的指令（LW、LH/LHU、LB/LBU）可以分别从数据存储器读取4个字节、2个字节、1个字节到通用寄存器中，相应的定义了STORE类型的指令（SW、SH、SB）可以分别保存通用寄存器的4个字节、2个字节、1个字节到数据存储器中。

（下面我们只实现4字节的LOAD/STORE。）


#### “2.6 Load and Store Instructions”

“RV32I is a load-store architecture, where only load and store instructions access memory and arithmetic instructions only operate on CPU registers. RV32I provides a 32-bit user address space that is byte-addressed and little-endian.”

RV32I属于load-store架构，只有load和store类型的指令访问存储器，运算类指令只能对CPU寄存器进行操作。

“Load and store instructions transfer a value between the registers and memory. Loads are encoded in the I-type format and stores are S-type. The effective byte address is obtained by adding register rs1 to the sign-extended 12-bit offset. Loads copy a value from memory to register rd. Stores copy the value in register rs2 to memory.”

存储器单元的地址：rs1的值（作为base）加上符号扩展的立即数（作为offset）。

LOAD/STORE指令格式：

![LOAD/STORE指令格式](images/tinycore03_01.png)


LOAD：将地址为(rs1+imm)的数据存储器单元的内容送rd，用符号表示为：rd <- [rs1+imm]

STORE：将rs2的数据写入地址为(rs1+imm)的数据存储器单元，用符号表示为：[rs1+imm] <- rs2

#### “Chapter 19 RV32/64G Instruction Set Listings”

LOAD类型的指令的inst[6:0] = 0b0000011

STORE类型的指令的inst[6:0] = 0b0100011

各指令详细格式：

![各指令详细格式](images/tinycore03_02.png)




## 例程设计

### 硬件实现

复制工程03_tinycore_step02到03_tinycore_step03，修改工程文件名称。

#### 增加数据存储器

参照前文增加ROM的方法增加RAM模块。

菜单“Tools/IP Core Generator”，选择SP：

![SP选择](images/tinycore03_03.png)

双击，进入配置界面：

![SP配置](images/tinycore03_04.png)

设置width=32，Depth=512。

表示一个存储单元保存32比特数据，该模块一共512个存储单元。

两次OK后，gowin_sp.v已加入工程，代码窗口的gowin_sp_tmp.v中提示了引用Gowin_SP模块的方法：
```
    Gowin_SP your_instance_name(
        .dout(dout_o), //output [31:0] dout
        .clk(clk_i), //input clk
        .oce(oce_i), //input oce
        .ce(ce_i), //input ce
        .reset(reset_i), //input reset
        .wre(wre_i), //input wre
        .ad(ad_i), //input [8:0] ad
        .din(din_i) //input [31:0] din
    );
```


修改core.v：

在端口中增加访问RAM的端口：
```
module core (
    input wire core_clk,
    input wire reset_n,
    //instruction ROM
    output wire [31:0] ibus_addr,
    output wire ibus_re,  //read enable
    input wire [31:0] instruction,
    //data RAM
    output wire ram_ce, //chip enable
    output wire ram_wre, //write enable
    output wire [8:0] ram_addr, //address bus
    input wire [31:0] ram_data_in,
    output wire [31:0] ram_data_out,
    //
    output wire [31:0] monitor_port
);
```

修改top.v：

参照前文增加ROM的方法增加RAM模块的引用。修改core的引用，增加端口。将core实例和ram实例连接起来。
```
    //core
    wire core_clk = counter[24];
    wire [31:0] ibus_addr;
    wire ibus_re;
    wire [31:0] instruction;
    wire [31:0] monitor_port;
    wire ram_ce;
    wire ram_wre;
    wire [8:0] ram_addr;
    wire [31:0] ram_data_in;
    wire [31:0] ram_data_out;

    core m_core (
        .core_clk(core_clk),
        .reset_n(reset_n),
        .ibus_addr(ibus_addr),
        .ibus_re(ibus_re),
        .instruction(instruction),
        .ram_ce(ram_ce),
        .ram_wre(ram_wre),
        .ram_addr(ram_addr),
        .ram_data_in(ram_data_in),
        .ram_data_out(ram_data_out),
        .monitor_port(monitor_port)
    );
```

```
    //data RAM
    Gowin_SP D_RAM(
        .dout(ram_data_in), //output [31:0] dout
        .clk(clk_200MHz), //input clk
        .oce(1'b1), //input oce
        .ce(ram_ce), //input ce
        .reset(~reset_n), //input reset
        .wre(ram_wre), //input wre
        .ad(ram_addr), //input [8:0] ad
        .din(ram_data_out) //input [31:0] din
    );
```



#### 指令译码

仅实现LW指令和SW指令。

从上面的文字可见，LW指令的opcode是OP-LOAD（0b0000011），func3为0b010，因此用如下代码来识别：

```
    wire opcode_LOAD = (opcode == 7'b0000011);
    wire opcode_STORE = (opcode == 7'b0100011);

    wire inst_LW = opcode_LOAD & (funct3 == 3'b010);
    wire inst_SW = opcode_STORE & (funct3 == 3'b010);
```


然后就是获取指令的操作数。

LW指令是将rs1的值加上imm得到存储器单元地址，取存储器单元内容，然后送rd。

SW指令是将rs1的值加上imm得到存储器单元地址，取rs2内容，然后送存储器单元。

注意：LW和SW的imm格式是不一样的。

获取rs1_idx：
```
    wire [4:0] i_rs1_idx = instruction[19:15];
```


LOAD指令获取imm，I格式的立即数是12位，位置在instruction[31:20]，最高位是符号位，需要将符号位进行扩展（复制），形成32位的有符号整数：
```
    wire [31:0] i_imm_I = {{21{instruction[31]}},instruction[30:20]};
```

获取rd_idx：
```
    wire [4:0] i_rd_idx = instruction[11:7];
```

获取rs2_idx：
```
    wire [4:0] i_rs2_idx = instruction[24:20];
```

STORE指令获取imm，S格式的立即数是12位，位置分别在instruction[31:25]和instruction[11:7]，最高位是符号位，需要将符号位进行扩展（复制），形成32位的有符号整数：
```
    wire [31:0] i_imm_S = {{21{instruction[31]}},instruction[30:25],instruction[11:7]};
```


#### 从Register File取操作数

同前。

#### 存储单元地址计算

rs1的值从regfile获得，立即数imm_I或imm_S从指令获得。地址计算语句为：
```
    wire [31:0] loadstore_addr = (inst_LW) ? rf_rs1data + i_imm_I :
                                 (inst_SW) ? rf_rs1data + i_imm_S : 32'b0;
```
当指令为LOAD类型时，addr=rs1+imm_I；当指令为STORE类型时，addr=rs1+imm_S。


#### 从数据存储器取数据

LOAD：激活数据存储器的控制总线和地址总线，从数据存储器的数据总线读取数据。

STORE：激活数据存储器的控制总线和地址总线、数据总线。

这里假设地址空间的0x00000000-0x000007ff区域分配给data ram。（后面会把外设区域分配在0x00000800-0x0000fff区域）

则ram_ce的表达式为：
```
    //assume data ram locate at address map 0x00000000 - 0x000003ff
    //reserve addr for peripheral at        0x00000800 - 0x00000fff
    //so that we can use the 12bit imm offset to address the data
    assign ram_ce = (loadstore_addr[31:11] == 21'b0) & (opcode_LOAD | opcode_STORE);
```

把loadstore_addr的bit[10:2]送data ram的地址线。为什么是bit[10:2]而不是bit[8:0]呢？留意一下data ram一个存储单元保存多少比特数据。
```
    assign ram_addr = loadstore_addr[10:2]; 
```

data ram的读写控制（core_clk的后半周期激活写允许）：
```
    assign ram_wre = inst_SW & (~core_clk);
```

当Gowin_SP的ce为高、oce为高时，dout输出地址为ad的存储单元的内容。

#### 回写rd

LOAD指令从存储器获取的数据要写回regfile，rd_idx从指令获得，过程与前述的ADDI指令过程类似。

因为ADI和LOAD都涉及到reg_file的数据回写，因此要修改原来ADI的回写代码：
```
//    assign rf_rd_data = adi_result;
//    assign rf_rdwen = inst_ADI;
    assign rf_rd_data = (inst_ADI) ? adi_result :
                          (inst_LW) ? ram_data_in : 32'b0;
    assign rf_rdwen = inst_ADI | inst_LW;
```


core_clk的下降沿触发回写。（看core对regfile的引用）

因为core_clk的上升沿触发PC值的改变，从而取下一条指令，因此半个core_clk的时长内要完成取指令、解释指令和执行指令（计算地址、访问RAM）的操作。

#### STORE的实现

SW的实现在decode阶段完成，将从regfile rs2得到的数据送ram_data_out。代码如下：
```
    assign ram_ce = (loadstore_addr[31:11] == 21'b0) & (opcode_LOAD | opcode_STORE);

    assign ram_wre = inst_SW & (~core_clk);

    assign ram_addr = loadstore_addr[10:2]; //loadstore_addr is in bytes but ram_addr is in words

    //output data from rs2
    assign ram_data_out = rf_rs2data;
```

#### 输出监视信号

```
    assign monitor_port = {ibus_addr[1:0], x16_value[3:0]};

```


### 软件实现

为了验证LOAD/STORE的功能实现，软件程序流程改为：
- 循环：
- 从存储单元m0读数据到x15；
- x15+1送x16；
- 将x16的值保存到存储单元m0；



#### 程序代码

程序代码在rom_data.mi中，4条指令。须重新生成gowin_prom。

```
#File_format=Hex
#Address_depth=8
#Data_width=32
00002783
00178813
01002023
ff5ff06f
00000000
00000000
00000000
00000000
```

或者直接修改gowin_prom.v：
```
defparam prom_inst_0.INIT_RAM_00 = 256'h00000000000000000000000000000000ff5ff06f010020230017881300002783;
```

#### 指令解读
汇编程序为：
LOOP: LW x15, 0(x0)
      ADDI x16, x15, #1
      SW x16, 0(x0)
      J LOOP

第1条指令：从RAM地址0处取值送x15。LW格式为<12b imm=0><5b rs1=0> func3=010 <5b rd=01111> opcode=0000011 

二进制值为：0b0000,0000,0000,0000,0010,0111,1000,0011。即0x00002783

对应汇编指令为：LW x15, 0(x0)

第2条指令：x15加1送x16。ADDI格式为<12b imm=1><5b rs1=01111> func3=000 <5b rd=10000> opcode=0010011

二进制值是：0b0000,0000,0001,0111,1000,1000,0001,0011。即0x00178813

对应汇编指令为：ADDI x16, x15, #1

第3条指令：将x16的值送RAM地址0单元。SW格式为<7b imm=0><5b rs2=10000><5b rs1=0> func3=010 <5b imm=0> opcode=0100011 

二进制值为：0b0000,0001,0000,0000,0010,0000,0010,0011。即0x01002023

对应汇编指令为：SW x16, 0(x0)

第4条指令：跳转到3条指令前即12字节。JAL格式为<imm[20|10:1|11|19:12]><5b rd=0> opcode=1101111

-12对应1..11,0100。注意bit0不在指令里。所以<imm[20|10:1|11|19:12]>值为1|1111111010|1|11111111

二进制值为：0b1111,1111,0101,1111,1111,0000,0110,1111。即0xff5ff06f

对应汇编指令为：JAL x0, -12

大概等效c语言程序为：
```
#define MEM_UNIT_0 *((volatile unsigned int *)0x40000000)

void main(void) {
  int i, j;
  while(1) {
    i = MEM_UNIT_0;
    j=i+1;
    MEM_UNIT_0 = j;
  }
}
```

#### 运行效果

```
    assign monitor_port = {ibus_addr[1:0], x16_value[3:0]};

```

大概1秒执行一条指令，每4条指令（一条加、一条跳转）更新一次x16，因此可以观察到led后4位约每4秒加1。led高2位显示指令地址，由00到11循环变化。

假如查看指令的类型和送出的地址信号：
```
assign monitor_port = {inst_J, inst_SW, inst_ADI, inst_LW, ibus_addr[1:0]};
```
可以看到各指令标志依次点亮。



