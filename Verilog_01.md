# Verilog入门（1）

#### 创建新工程

最简单的办法是复制整个旧工程目录然后改目录名，然后打开工程进行修改。

下面介绍从无到有用文本编辑器创建一个简单的工程。

- 创建一个目录，目录名字要反映工程内容。

- 在工程目录中，创建工程文件：<工程名>.gprj，输入内容：

```
<?xml version="1" encoding="UTF-8"?>
<!DOCTYPE gowin-fpga-project>
<Project>
    <Template>FPGA</Template>
    <Version>5</Version>
    <Device name="GW1NR-9C" pn="GW1NR-LV9QN88PC6/I5">gw1nr9c-004</Device>
    <FileList>
        <File path="src/top.v" type="file.verilog" enable="1"/>
        <File path="src/tangnano9k.cst" type="file.cst" enable="1"/>
    </FileList>
</Project>
```

- 在工程目录中，创建“src”子目录；
- 在src子目录下，创建文件top.v；（注：文件名top可以修改，但需要和.gprj工程文件中的源码文件名一致。）
- 编辑top.v，输入：
```
module top
(
    output wire led0
);

    assign led0 = 0;

endmodule 
```
- 在src子目录下，创建文件tangnano9k.cst；（注：文件名tangnano9k可以修改，但需要和.gprj工程文件中的.cst文件名一致。）
- 编辑tangnano9k.cst，输入：
```
IO_LOC "led0" 10;
IO_PORT "led0" PULL_MODE=UP DRIVE=8;
```

#### Verilog源文件

Verilog源文件以.v为后缀，里面包含一个或多个module（模块）结构。

1、module

我们查看芯片手册的时候经常看到芯片内部结构的方框图，通过方框、一些典型的元件符号、方框之间以及方框与元件符号之间的连接来表示电路的结构。

这里的“module（模块）”就用来代表一个方框。从module语句到endmodule之间的语句用来描述方框的内部结构。

方框里面可以包含方框，因此module内部可以包含module（通过直接引用模块名字来包含，不出现module关键字）。


```
module top
(
    output wire led0
);
```
module后面的“top”是模块的名字。

小括号括起来的是模块的端口。注意端口之间用逗号分隔，最后一个端口后面没有逗号。小括号后面有个分号。

2、端口（port）

术语“端口（port）”指的是模块和其它元件或模块之间可以发生连接的接口。

output是关键字，表示端口的输入输出属性，其它输入输出属性关键字还有input和inout。

3、wire

wire是关键字，表示一段连线。wire后面的led0是标识连线的变量/标签（相当于原理图EDA软件中的label）。

Verilog规范中的数据类型分为两大类：net和variable。wire是net大类中常见的一种类型，用于一对一或者一对多（连接一个输出端口（驱动）和多个输入端口的网络。）


4、assign

assign是关键字，与wire类型的变量搭配，表示该wire变量的值或表达式，用来描述组合逻辑电路。

```
    assign led0 = 0;
```
这一语句表示将led0接地。

5、endmodule

关键字endmodule表示模块的结束。




#### 引脚设置（.cst文件）


```
IO_LOC "led0" 10;
IO_PORT "led0" PULL_MODE=UP DRIVE=8;
```

关键字IO_LOC表示定义top模块端口对应的FPGA芯片引脚。led0是top模块的端口名字，10是芯片引脚号。

引脚号通过查看模块手册或原理图获得。

关键字IO_PORT表示描述端口的属性，PULL_MODE=UP表示上拉，DRIVE表示驱动电流强度。

通过GOWIN软件菜单上的：Tools/FloorPlanner，可以设置引脚上的更多参数。


#### 效果及理解

综合及烧写，可以看到并排的6个led灯中的一个点亮。（注：另外一个常亮的led是电源指示）

将“assign led0=0;” 改为 “assign led0=1;”，即接高电平。烧写，可以发现灯灭。即led是低电平亮。

这里再讨论一下对top module和引脚的理解。

对照大家比较熟悉的电路板（PCB或者面包板、洞洞板），module就相当于一块电路板，top module就是最大的那块电路板。大电路板上可以有芯片、电阻电容等元件，还可以有小电路板，这就相当于module里面可以包含组合逻辑电路、时序逻辑电路和其它module。

电路板上有一些排针或接插件，可以通过杜邦线或接插件连接到其它电路板上，这些排针或接插件上的每个端子，就相当于module上的port（端口）。

FPGA芯片相当于装电路板的外壳，外壳上有对外连接的端子，就相当于FPGA芯片的引脚。.cst文件中定义的top module的端口和FPGA引脚的对应关系，就相当于在盒子里面用杜邦线将最大的那块电路板的端子和外壳上的端子内侧连接起来。

因此，如果两个工程用到的FPGA引脚是一样的，它们就可以重用同一个.cst文件和top module的端口声明列表。注意：top module的端口声明列表中的端口数目和.cst中的要保持一致。

然后我们就重点关注module里面的代码即内部电路结构就好。
