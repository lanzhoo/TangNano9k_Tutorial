-series GW1NR
-device GW1NR-9C
-package QFN88P
-part_number GW1NR-LV9QN88PC6/I5


-mod_name Gowin_SP
-file_name gowin_sp
-path C:/hzheng/TangNano9k/gitee_project/03_tinycore_step03/src/gowin_sp/
-type RAM_SP
-file_type vlg
-depth 512
-width 32
-read_mode bypass
-write_mode normal
-reset_mode sync