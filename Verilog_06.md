# Verilog入门（6）:CASE语句和有限状态机（FSM）

本节通过例子介绍有限状态机（FSM）的实现，其中用到CASE语句。

本节例子是在Verilog入门（5）的例子基础上修改，增加以下功能：
- 通过串口接收上位机数据；
- 将数据通过串口回传上位机；

有限状态机（FSM）有一段式、两段式、三段式等多种实现方式，大家自行搜索学习。据说初学者用三段式比较好。本节中将PicoSoC项目中的simpleuart.v中的一段式修改为三段式进行介绍。

我在这方面的学习还不透彻，仅供参考。

注：本节例程中使用的串口代码是参考PicoSoC项目中的源代码简化而来（原来的copyright在例子工程代码中有）。

### 操作过程

#### 在工程中添加.v文件

将前文的02_verilog_step04例子工程复制到新目录，改目录名，改工程文件名。

在工程中增加uart_rx.v。

#### 串口接收代码

编辑uart_rx.v，输入以下代码：
```
module uart_rx #(parameter BAUDRATE_DIVIDER=234) (
    input wire clk,
    input wire reset_n,
    input wire rxd,
    output wire [7:0] rx_data,
    output wire rx_flag
);

    reg [31:0] recv_divcnt;
	reg [7:0] recv_pattern;
	reg [7:0] recv_buf_data;
	reg recv_buf_valid;

    //FSM section 1
    localparam S_IDLE = 0, S_RX_START = 1, S_RX_DATA=2, S_RX_STOP_BIT=10;
    reg [3:0] current_state = 0, next_state;    

    always @(posedge clk or negedge reset_n) begin
        if (!reset_n) begin
            current_state <= S_IDLE;
        end else begin
            current_state <= next_state;
        end
    end

    //FSM section 2
    always @(*) begin
        next_state = 0;

        case (current_state) 
            S_IDLE: begin
                if (!rxd) begin  
                    next_state <= S_RX_START;
                end else begin
                    next_state <= S_IDLE;                        
                end
            end
            S_RX_START : begin
                if (2*recv_divcnt > BAUDRATE_DIVIDER) begin
                    next_state <= S_RX_DATA;
                end else begin
                    next_state <= S_RX_START;
                end
            end
            S_RX_STOP_BIT : begin
                if (recv_divcnt > BAUDRATE_DIVIDER) begin
                    next_state <= S_IDLE;
                end else begin
                    next_state <= S_RX_STOP_BIT;
                end
            end
            default: begin
                if (recv_divcnt > BAUDRATE_DIVIDER) begin
                    next_state <= current_state + 1'b1;
                end else begin
                    next_state <= current_state;
                end
            end
        endcase
    end

    //FSM section 3
	always @(posedge clk) begin
		if (!reset_n) begin
			recv_divcnt <= 0;
			recv_pattern <= 0;
			recv_buf_data <= 0;
			recv_buf_valid <= 0;
		end else begin
			recv_divcnt <= recv_divcnt + 1;
			recv_buf_valid <= 0;
            case (current_state) 
                S_IDLE: begin
                    recv_divcnt <= 0;
                end
                S_RX_START : begin
                    if (2*recv_divcnt > BAUDRATE_DIVIDER) begin
                        recv_divcnt <= 0;
                    end
                end
                S_RX_STOP_BIT : begin
                    if (recv_divcnt > BAUDRATE_DIVIDER) begin
                        recv_buf_data <= recv_pattern;
                        recv_buf_valid <= 1;
                    end
                end
                default: begin
                    if (recv_divcnt > BAUDRATE_DIVIDER) begin
                        recv_pattern <= {rxd, recv_pattern[7:1]};
                        recv_divcnt <= 0;
                    end
                end
            endcase
        end
    end

    //output
    assign rx_data = recv_buf_data;
    assign rx_flag = recv_buf_valid;

endmodule


```

#### 引用串口接收模块

在top.v中，增加uart0_rxd端口：
```
module top
(
    input wire [1:0] button,
    input wire sys_clk,
    input uart0_rxd,          
    output uart0_txd,
    output wire [5:0] led
);
```

在.cst文件中，增加uart0_rxd到引脚的关联：
```
IO_LOC "uart0_rxd" 18;
IO_PORT "uart0_rxd" IO_TYPE=LVCMOS33 PULL_MODE=UP;
```

在top.v中，增加引用uart_rx模块的代码：
```
wire [7:0] uart_rx_data;
wire uart_rx_flag;

uart_rx #(.BAUDRATE_DIVIDER(234)) u_uart0_rx(  
   .clk(sys_clk),  //27MHz
   .reset_n(reset_n),
   .rxd(uart0_rxd),
   .rx_data(uart_rx_data),
   .rx_flag(uart_rx_flag)
);
```

将原来的uart_tx发送触发代码修改为定时发送和接收回传均可触发：
```
//wire uart_tx_en = (counter2[16:0] == 17'b1); //trigger tx every 1s
//wire [7:0] uart_tx_data = {~button, ~led};
wire uart_tx_en1 = (counter2[16:0] == 17'b1); //trigger tx every 1s
wire [7:0] uart_tx_data1 = {~button, ~led};

wire uart_tx_en = uart_rx_flag | uart_tx_en1;
wire [7:0] uart_tx_data = uart_rx_flag ? uart_rx_data : uart_tx_data1;
```


#### 观察效果

烧写。用串口助手观察，波特率设为115200，无奇偶校验。勾选十六进制显示和十六进制发送。发送数据，可以看到在定时上报数据的同时，能收到回传的发送数据。

### 代码理解

#### 三段式FSM代码

在uart_rx.v中，第一段代码用于声明状态：
```
    localparam S_IDLE = 0, S_RX_START = 1, S_RX_DATA=2, S_RX_STOP_BIT=10;
    reg [3:0] current_state = 0, next_state;    

    always @(posedge clk or negedge reset_n) begin
        if (!reset_n) begin
            current_state <= S_IDLE;
        end else begin
            current_state <= next_state;
        end
    end
```
有两个变量current_state和next_state，在clk的上升沿进行新状态的更新。

这里的代码可以作为模板，除了更新状态常量的名字，其它可以保留。


第二段代码用于描述状态的迁移，根据当前状态以及其它输入信号决定next_state的状态值：
```
    always @(*) begin
        next_state = 0;

        case (current_state) 
            S_IDLE: begin
                if (!rxd) begin  
                    next_state <= S_RX_START;
                end else begin
                    next_state <= S_IDLE;                        
                end
            end
            S_RX_START : begin
                if (2*recv_divcnt > BAUDRATE_DIVIDER) begin
                    next_state <= S_RX_DATA;
                end else begin
                    next_state <= S_RX_START;
                end
            end
            S_RX_STOP_BIT : begin
                if (recv_divcnt > BAUDRATE_DIVIDER) begin
                    next_state <= S_IDLE;
                end else begin
                    next_state <= S_RX_STOP_BIT;
                end
            end
            default: begin
                if (recv_divcnt > BAUDRATE_DIVIDER) begin
                    next_state <= current_state + 1'b1;
                end else begin
                    next_state <= current_state;
                end
            end
        endcase
    end
```
这里出现了case语句结构：
```
case (<状态变量>)
    <值1>: begin
        <代码>
    end
    <值2>: begin
        <代码>
    end
    ...
    default: begin
        <代码>
    end
endcase   
```
注意：default必须存在。

第三段代码用于描述当前状态的输出信号：
```
	always @(posedge clk) begin
		if (!reset_n) begin
			recv_divcnt <= 0;
			recv_pattern <= 0;
			recv_buf_data <= 0;
			recv_buf_valid <= 0;
		end else begin
			recv_divcnt <= recv_divcnt + 1;
			recv_buf_valid <= 0;
            case (current_state) 
                S_IDLE: begin
                    recv_divcnt <= 0;
                end
                S_RX_START : begin
                    if (2*recv_divcnt > BAUDRATE_DIVIDER) begin
                        recv_divcnt <= 0;
                    end
                end
                S_RX_STOP_BIT : begin
                    if (recv_divcnt > BAUDRATE_DIVIDER) begin
                        recv_buf_data <= recv_pattern;
                        recv_buf_valid <= 1;
                    end
                end
                default: begin
                    if (recv_divcnt > BAUDRATE_DIVIDER) begin
                        recv_pattern <= {rxd, recv_pattern[7:1]};
                        recv_divcnt <= 0;
                    end
                end
            endcase
        end
    end
```

#### case结构语句

前面已经写了，这里重复一下：
```
case (<状态变量>)
    <值1>: begin
        <代码>
    end
    <值2>: begin
        <代码>
    end
    <值3>: begin
        <代码>
    end
    ...
    default: begin
        <代码>
    end
endcase   
```
注意：default必须存在。

在有的采用独热码表示状态变量（即每个状态值中只有1比特为1其它都为0）的FSM实现中，case结构的形式为：
```
case (1'b1)
    <状态变量>[0]: begin
        <代码>
    end
    <状态变量>[1]: begin
        <代码>
    end
    <状态变量>[2]: begin
        <代码>
    end
    ...
    default: begin
        <代码>
    end
endcase   
```

#### 串口接收代码

这段实现代码是假设信号非常理想下的很简单的实现代码，没有做各种确认。

1、当输入线拉低时表示帧的开始：
```
                if (!rxd) begin  
                    next_state <= S_RX_START;
```

2、传入的参数BAUDRATE_DIVIDER表示一比特数据包含多少个clk。数BAUDRATE_DIVIDER/2个clk表示到了开始比特的中间位置：
```
                if (2*recv_divcnt > BAUDRATE_DIVIDER) begin
                    next_state <= S_RX_DATA;
```

3、状态值为2到9这段，代码看default。数BAUDRATE_DIVIDER个clk，然后采样得到一位数据。
```
            default: begin
                if (recv_divcnt > BAUDRATE_DIVIDER) begin
                    next_state <= current_state + 1'b1;
```
```
                default: begin
                    if (recv_divcnt > BAUDRATE_DIVIDER) begin
                        recv_pattern <= {rxd, recv_pattern[7:1]};
                        recv_divcnt <= 0;
                    end
                end
```
其中，语句：
```
                        recv_pattern <= {rxd, recv_pattern[7:1]};
```
将新的比特数据移入缓冲区recv_pattern。

4、状态值为10时，接收结束比特，状态回到IDLE。将缓冲区数据送recv_buf_data，设recv_buf_valid为有效（持续一个clk）。
```
            S_RX_STOP_BIT : begin
                if (recv_divcnt > BAUDRATE_DIVIDER) begin
                    next_state <= S_IDLE;
                end else begin
                    next_state <= S_RX_STOP_BIT;
                end
            end
```
```
                S_RX_STOP_BIT : begin
                    if (recv_divcnt > BAUDRATE_DIVIDER) begin
                        recv_buf_data <= recv_pattern;
                        recv_buf_valid <= 1;
                    end
                end
```


