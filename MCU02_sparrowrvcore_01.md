【 [返回主页](index.md) 】

# 另一个完整MCU例子：基于SparrowRV core + JTAG + MounRiver_Studio

例程目录：[06_mcu02_sparrowrv_01](06_mcu02_sparrowrv_01)；其中c工程代码：06_mcu02_sparrowrv_01/c_mrs_prj


### 概述

本工程从TangPrimer20kDock的实现方案移植，更改了IROM的大小（从64k改为32k）。另外，因为TangNano9k模块上只有两个按钮，都用作reset了，因此外设中的button不起作用。

具体使用参照：
https://gitee.com/lanzhoo/tang-primer20k_-tutorial/blob/master/mcu/C01_intro.md

该工程方案比前一MCU方案更完善的地方在于实现了中断。

#### 硬件

硬件连接：
- jtag_TCK接25脚；
- jtag_TMS接26脚；
- jtag_TDI接27脚；
- jtag_TDO接28脚；
- jtag_GND接GND；

![JTAG模块连接照片](images/mcu01_01.png)

资源占用：
![资源占用](images/mcu02_01.png)


#### 软件操作

CMD窗口1效果：
![openOCD窗口](images/mcu02_02.png)

CMD窗口2效果：
![telnet窗口](images/mcu02_03.png)



