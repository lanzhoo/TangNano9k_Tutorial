# 基于Aruduino IDE的设计（4）：c中的HAL层led和uart tx函数

上节的led显示是将x16寄存器的内容引出，本节改回用GPIO模块控制。

然后将GPIO LED显示和UART TX代码整理为HAL层函数，演示代码分层的概念。

## FPGA硬件设计

复制工程04_arduino_step02到04_arduino_step03，修改工程文件名字。需要重新设置System Verilog2017选项。

### 修改led输出来源

top.v：
```
//    assign led = ~monitor_port[5:0];
    assign led = peri_led;

```


## 软件部分

### 外设I/O寄存器地址

由硬件设计的memory map，外设I/O寄存器的内存映射范围为:0x0000-0x03ff。

core.v：
```
    //to use the 12bit signed imm offset to address the data for optimization of the code
    //assign addr for peripheral at         0x????0000 - 0x????03ff (1kB)
    //assign addr for data ram at           0x????0400 - 0x????13ff (4kB)
    assign peripheral_ce = (loadstore_addr[15:10] == 6'b0) & (opcode_LOAD | opcode_STORE);
    assign ram_ce = (loadstore_addr[15:13] == 3'b0) & (loadstore_addr[12:10] != 3'b0) & (opcode_LOAD | opcode_STORE);

```

```
    assign peripheral_wre = opcode_STORE & (~core_clk);
    assign peripheral_addr = {1'b0, loadstore_addr[9:2]}; //loadstore_addr is in bytes but peripheral_addr is in words
    //output data from rs2
    assign peripheral_data_out = rf_rs2data;
```

    
其中，分配给gpio的范围是0x0000-0x000f，共4个32位地址；分配给uart的范围是0x0010-0x001f，共4个32位地址。

peripheral.v：
```
    //gpio
    //
    wire gpio_ce = (ce & (addr[8:2] == 7'b0)) ? 1'b1 : 1'b0;

```
```
    //uart
    //
    wire uart_ce = (ce & (addr[8:2] == 7'b0000001)) ? 1'b1 : 1'b0;

```

GPIO的输出寄存器的地址为0x0000。gpio.v：
```
    always @(posedge clk) begin
        if (~reset_n) begin
            data_out_reg <= 32'b0;
        end
        else if (ce & wre & (addr==2'b00)) begin
            data_out_reg <= data_in;
        end
    end    
```

uart的发送寄存器的地址为0x0010。peripheral_uart.v：
```
    reg [31:0] data_tx_reg;

    wire uart_tx_en = ce & wre & (addr==2'b00);

    //write data_tx_reg register
    always @(posedge clk) begin
        if (~reset_n) begin
            data_tx_reg <= 32'b0;
        end
        else if (uart_tx_en) begin
            data_tx_reg <= data_in;
        end
    end    
```

### Arduino代码

#### 方案一

方案一：直接在.ino文件的loop()中实现。（样例在Blink01.ino）

```
#define GPIO_ODR      *((volatile unsigned int *)0x00000000)
#define UART0_TX_Send *((volatile unsigned int *)0x00000010)

unsigned int gCount;

void setup() {
  // put your setup code here, to run once:
  gCount = 0;
}

void loop() {
  // put your main code here, to run repeatedly: 
  unsigned int i;
  
  GPIO_ODR = gCount;
  UART0_TX_Send = gCount;
  
  gCount++;
  
  for (i=0;i<10;i++) {
    asm volatile(
      "nop;"
    );
  }
}
```

观察效果：

- 烧写；
- Arduino下载；

可以看到led逐步加一。用串口助手（波特率115200）观察，可以周期性收到递增的数据。

反汇编：
```
80002000 <_start>:
80002000:	00001137          	lui	sp,0x1
80002004:	03c0006f          	j	80002040 <main>

80002008 <_fini>:
80002008:	0000006f          	j	80002008 <_fini>

8000200c <setup>:
8000200c:	40002223          	sw	zero,1028(zero) # 404 <gCount>
80002010:	00008067          	ret

80002014 <loop>:
80002014:	40402703          	lw	a4,1028(zero) # 404 <gCount>
80002018:	00e02023          	sw	a4,0(zero) # 0 <__FRAME_END__-0x400>
8000201c:	00e02823          	sw	a4,16(zero) # 10 <__FRAME_END__-0x3f0>
80002020:	40402703          	lw	a4,1028(zero) # 404 <gCount>
80002024:	00170713          	addi	a4,a4,1
80002028:	40e02223          	sw	a4,1028(zero) # 404 <gCount>
8000202c:	00a00793          	li	a5,10
80002030:	00000013          	nop
80002034:	fff78793          	addi	a5,a5,-1
80002038:	fe079ce3          	bnez	a5,80002030 <loop+0x1c>
8000203c:	00008067          	ret

80002040 <main>:
80002040:	ff010113          	addi	sp,sp,-16 # ff0 <__global_pointer$+0x3ec>
80002044:	00112623          	sw	ra,12(sp)
80002048:	fc5ff0ef          	jal	ra,8000200c <setup>
8000204c:	fc9ff0ef          	jal	ra,80002014 <loop>
80002050:	ffdff06f          	j	8000204c <main+0xc>

Disassembly of section .eh_frame:

00000400 <__FRAME_END__>:
 400:	0000                	.2byte	0x0
	...

Disassembly of section .sbss:

00000404 <gCount>:
 404:	0000                	.2byte	0x0

```

分析代码：
- 对应“GPIO_ODR = gCount;”的是：
```
80002014:	40402703          	lw	a4,1028(zero) # 404 <gCount>
80002018:	00e02023          	sw	a4,0(zero) # 0 <__FRAME_END__-0x400>
```
- 对应“UART0_TX_Send = gCount;”的是：
```
8000201c:	00e02823          	sw	a4,16(zero) # 10 <__FRAME_END__-0x3f0>
```
- 对应“gCount++;”的是：
```
80002020:	40402703          	lw	a4,1028(zero) # 404 <gCount>
80002024:	00170713          	addi	a4,a4,1
80002028:	40e02223          	sw	a4,1028(zero) # 404 <gCount>
```

#### 方案二

方案二：在.ino文件中实现，提炼出控制函数。（样例在Blink02.ino）

```
#define GPIO_ODR      *((volatile unsigned int *)0x00000000)
#define UART0_TX_Send *((volatile unsigned int *)0x00000010)

void modSetLed(volatile unsigned int vData);
void modUartSend(volatile unsigned int vData);

unsigned int gCount;

void setup() {
  // put your setup code here, to run once:
  gCount = 0;
}

void loop() {
  unsigned int i;

  modSetLed(gCount);
  modUartSend(gCount);

  gCount++;
  
  for (i=0;i<10;i++) {
    asm volatile(
      "nop;"
    );
  }
}
void modSetLed(volatile unsigned int vData) {
  GPIO_ODR = vData;
}
void modUartSend(volatile unsigned int vData) {
  UART0_TX_Send = vData;
}
```

代码中，将硬件相关代码封装在modSetLed()和modUartSend()中。

注意：modSetLed()的输入参数加了“volatile”声明，否则“GPIO_ODR = vData;”这句会被优化掉不见了。（这也可能是编译器的bug，因为modUartSend()的输入参数可以不加“volatile”，它们的区别只是GPIO_ODR的地址为零。）

对应的反汇编代码为：
```
80002014 <loop>:
80002014:	40402703          	lw	a4,1028(zero) # 404 <gCount>
80002018:	00e02023          	sw	a4,0(zero) # 0 <__FRAME_END__-0x400>
8000201c:	00e02823          	sw	a4,16(zero) # 10 <__FRAME_END__-0x3f0>
80002020:	40402703          	lw	a4,1028(zero) # 404 <gCount>
80002024:	00170713          	addi	a4,a4,1
80002028:	40e02223          	sw	a4,1028(zero) # 404 <gCount>
8000202c:	00a00793          	li	a5,10
80002030:	00000013          	nop
80002034:	fff78793          	addi	a5,a5,-1
80002038:	fe079ce3          	bnez	a5,80002030 <loop+0x1c>
8000203c:	00008067          	ret
```
从中可以看到，函数调用被用inline方式优化掉了。

假如把函数声明加上noinline属性，改为：
```
void modSetLed(volatile unsigned int vData) __attribute__((noinline));
```

则反汇编代码为：
```
80002014 <_Z9modSetLedj>:
80002014:	ff010113          	addi	sp,sp,-16 # ff0 <__global_pointer$+0x3ec>
80002018:	00a12623          	sw	a0,12(sp)
8000201c:	00c12783          	lw	a5,12(sp)
80002020:	00f02023          	sw	a5,0(zero) # 0 <__FRAME_END__-0x400>
80002024:	01010113          	addi	sp,sp,16
80002028:	00008067          	ret

8000202c <loop>:
8000202c:	ff010113          	addi	sp,sp,-16
80002030:	00112623          	sw	ra,12(sp)
80002034:	00812423          	sw	s0,8(sp)
80002038:	00912223          	sw	s1,4(sp)
8000203c:	40402403          	lw	s0,1028(zero) # 404 <gCount>
80002040:	00040513          	mv	a0,s0
80002044:	fd1ff0ef          	jal	ra,80002014 <_Z9modSetLedj>
```
出现了调用的代码以及压栈出栈的操作，显然增加了许多代码量。


#### 方案三

方案三：为体现良好的代码架构，在cores目录下实现hal层函数，在.ino文件中只包含硬件无关代码，调用hal层函数实现功能。（样例在Blink03.ino和cores目录下文件）

在arduino安装目录的hardware子目录下的“riscv\tangnano9k\cores\riscv\”下创建halTangnanoTiny.h和halTangnanoTiny.c。

“hardware\riscv\tangnano9k\cores\riscv\halTangnanoTiny.h”：
```
#ifndef _HAL_TANGNANO_TINY_H
#define _HAL_TANGNANO_TINY_H

#ifdef  __cplusplus
extern "C" {
#endif

extern void halSetLed(volatile unsigned int vData);
extern void halUartSend(volatile unsigned int vData);
extern void halDelay(unsigned int vData);

#ifdef  __cplusplus
}
#endif

#endif
```

“hardware\riscv\tangnano9k\cores\riscv\halTangnanoTiny.c”：
```
#include "halTangnanoTiny.h"

#define GPIO_ODR      *((volatile unsigned int *)0x00000000)
#define UART0_TX_Send *((volatile unsigned int *)0x00000010)


void halSetLed(volatile unsigned int vData){          
  GPIO_ODR = vData;
}

void halUartSend(volatile unsigned int vData){          
  UART0_TX_Send = vData;
}

void halDelay(unsigned int vData){
  if (vData == 0)
    return;
  unsigned int i;
  for (i=0;i<vData;i++) {
    asm volatile(
      "nop;"
    );
  }
}
```

在Arduino IDE环境下创建.ino文件，代码：
```
#include "halTangnanoTiny.h"

unsigned int gCount;

void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:
  gCount++;
  halSetLed(gCount);
  halUartSend(gCount);

  halDelay(10);
}
```

从.ino文件可以看出，实现了.ino代码与硬件无关，只包含业务逻辑。

这时候的反汇编代码为：
```
80002000 <_start>:
80002000:	00001137          	lui	sp,0x1
80002004:	0900006f          	j	80002094 <main>

80002008 <_fini>:
80002008:	0000006f          	j	80002008 <_fini>

8000200c <setup>:
8000200c:	00008067          	ret

80002010 <loop>:
80002010:	ff010113          	addi	sp,sp,-16 # ff0 <__global_pointer$+0x3ec>
80002014:	00112623          	sw	ra,12(sp)
80002018:	00812423          	sw	s0,8(sp)
8000201c:	40402503          	lw	a0,1028(zero) # 404 <gCount>
80002020:	00150513          	addi	a0,a0,1
80002024:	40a02223          	sw	a0,1028(zero) # 404 <gCount>
80002028:	024000ef          	jal	ra,8000204c <halSetLed>
8000202c:	40402503          	lw	a0,1028(zero) # 404 <gCount>
80002030:	034000ef          	jal	ra,80002064 <halUartSend>
80002034:	00a00513          	li	a0,10
80002038:	044000ef          	jal	ra,8000207c <halDelay>
8000203c:	00c12083          	lw	ra,12(sp)
80002040:	00812403          	lw	s0,8(sp)
80002044:	01010113          	addi	sp,sp,16
80002048:	00008067          	ret

8000204c <halSetLed>:
8000204c:	ff010113          	addi	sp,sp,-16
80002050:	00a12623          	sw	a0,12(sp)
80002054:	00c12783          	lw	a5,12(sp)
80002058:	00f02023          	sw	a5,0(zero) # 0 <__FRAME_END__-0x400>
8000205c:	01010113          	addi	sp,sp,16
80002060:	00008067          	ret

80002064 <halUartSend>:
80002064:	ff010113          	addi	sp,sp,-16
80002068:	00a12623          	sw	a0,12(sp)
8000206c:	00c12783          	lw	a5,12(sp)
80002070:	00f02823          	sw	a5,16(zero) # 10 <__FRAME_END__-0x3f0>
80002074:	01010113          	addi	sp,sp,16
80002078:	00008067          	ret

8000207c <halDelay>:
8000207c:	00050a63          	beqz	a0,80002090 <halDelay+0x14>
80002080:	00000793          	li	a5,0
80002084:	00000013          	nop
80002088:	00178793          	addi	a5,a5,1
8000208c:	fef51ce3          	bne	a0,a5,80002084 <halDelay+0x8>
80002090:	00008067          	ret

80002094 <main>:
80002094:	ff010113          	addi	sp,sp,-16
80002098:	00112623          	sw	ra,12(sp)
8000209c:	f71ff0ef          	jal	ra,8000200c <setup>
800020a0:	f71ff0ef          	jal	ra,80002010 <loop>
800020a4:	ffdff06f          	j	800020a0 <main+0xc>

Disassembly of section .eh_frame:

00000400 <__FRAME_END__>:
 400:	0000                	.2byte	0x0
	...

Disassembly of section .sbss:

00000404 <gCount>:
 404:	0000                	.2byte	0x0
```

Arduino的编译日志部分代码为：
```
...

正在编译项目...
riscv-none-elf-gcc ... Blink03.ino.cpp -o Blink03.ino.cpp.o
Compiling libraries...
Compiling core...
riscv-none-elf-gcc ... halTangnanoTiny.c -o halTangnanoTiny.c.o
riscv-none-elf-gcc ... main.c -o main.c.o
riscv-none-elf-ar rcs core.a halTangnanoTiny.c.o 
riscv-none-elf-ar rcs core.a main.c.o

Linking everything together...
riscv-none-elf-gcc -T Reindeer.ld... --specs=nosys.specs -march=rv32i -mabi=ilp32 -o Blink03.ino.elf Blink03.ino.cpp.o core.a
...
```
从中可见，halTangnanoTiny.c.o和main.c.o一起打包到core.a，再和Blink03.ino.cpp.o链接称为.elf文件。


分析一下反汇编代码中的新指令：
```
80002034:	00a00513          	li	a0,10
```
opcode=7'b0010011，是一条ADDI指令。

```
8000207c:	00050a63          	beqz	a0,80002090 <halDelay+0x14>
```
opcode=7'b1100011，func3=3'b000，rs1=20，rs2=0，是一条BEQ指令。

这条指令目前还没实现，但跳过这条指令不影响程序执行。




