# Verilog入门（5）:模块的引用

本节用例子介绍如何在module的代码中包含另一个module的实例。（在规范中称为模块的实例化）

本节例子是在Verilog入门（3）的跑马灯例子基础上修改，在跑马灯功能基础上增加以下功能：
- 通过串口定时发送按键状态和led状态；

注：本节例程中使用的串口代码是参考PicoSoC项目中的源代码简化而来（原来的copyright在例子工程代码中有）。

### 操作过程

#### 在工程中添加.v文件

将前文的跑马灯例子工程复制到新目录，改目录名，改工程文件名。

在工程中添加.v文件有多种方法：

1、方法1
- 启动云源软件，打开工程（双击工程文件可直接打开）；
- 在“Design”窗口，点选“Verilog Files”，按鼠标右键；
- 在右键菜单中选择“New File”；
- 选“Verilog File”，点“OK”；
- 输入名称，勾选“Add to current project”，“OK”；

2、方法2
- 用文本编辑器创建.v文件；
- 启动云源软件，打开工程（双击工程文件可直接打开）；
- 在“Design”窗口，点选“Verilog Files”，按鼠标右键；
- 在右键菜单中选择“Add Files”；
- 选择文件，点“打开”；

3、方法3
- 用文本编辑器创建.v文件；
- 用文本编辑器编辑工程文件，加入新的.v文件；

#### 串口发送代码

按前述方法在工程中增加uart_tx.v。

输入以下代码：
```
module uart_tx #(parameter BAUDRATE_DIVIDER=234) (
    input wire clk,
    input wire reset_n,
    input wire tx_en,
    input wire [7:0] tx_data,
    output wire txd
);
    localparam FRAME_LEN = 10;

    reg [FRAME_LEN-1:0] send_pattern;
    reg [3:0] send_bitcnt;
    reg [31:0] send_divcnt;

    assign txd = send_pattern[0];

    always @(posedge clk) begin
        send_divcnt <= send_divcnt + 1;
        if (!reset_n) begin
            send_pattern <= ~0;
            send_bitcnt <= 0;
            send_divcnt <= 0;
        end else begin
            if (tx_en && !send_bitcnt) begin
                send_pattern <= {1'b1, tx_data[7:0], 1'b0};
                send_bitcnt <= FRAME_LEN;
                send_divcnt <= 0;
            end 
            else if ((send_divcnt >= BAUDRATE_DIVIDER) && send_bitcnt) begin
                send_pattern <= {1'b1, send_pattern[9:1]};
                send_bitcnt <= send_bitcnt - 1'b1;
                send_divcnt <= 0;
            end
        end
    end
endmodule

```

#### 引用串口模块

在top.v中，增加uart0_txd端口：
```
module top
(
    input wire [1:0] button,
    input wire sys_clk,
    output uart0_txd,         // usb uart
    output wire [5:0] led
);
```

在.cst文件中，增加uart0_txd到引脚的关联：
```
IO_LOC "uart0_txd" 17;
IO_PORT "uart0_txd" IO_TYPE=LVCMOS33 PULL_MODE=UP DRIVE=8;
```

在top.v中，增加引用uart_tx模块的代码：
```
wire uart_tx_en = (counter2[16:0] == 17'b1); //trigger tx every 1s
wire [7:0] uart_tx_data = {~button, ~led};

uart_tx #(.BAUDRATE_DIVIDER(234)) u_uart0_tx(  
   .clk(sys_clk),
   .reset_n(reset_n),
   .tx_en(uart_tx_en),
   .tx_data(uart_tx_data),
   .txd(uart0_txd)
);
```

#### 观察效果

烧写。用串口助手观察，波特率设为115200。没有按下button0时的数据为：
```
03 06 0C 18 30 21 03 06 0C 18 30 21
```
按下button0时的数据为：
```
43 46 4C 58 70 61 43 46 4C 58 70 61
```
按下button1时为reset，不会收到数据。

将参数BAUDRATE_DIVIDER的值改为2812，串口助手的波特率改为9600。同样可以收到数据。
```
//Baudrate=9600; DIVIDER=27,000,000/9600=2812.5
uart_tx #(.BAUDRATE_DIVIDER(2812)) u_uarttx(  
```

### 代码理解

#### 引用模块

引用串口模块的代码为：
```
uart_tx #(.BAUDRATE_DIVIDER(234)) u_uart0_tx(  
   .clk(sys_clk),  //27MHz
   .reset_n(reset_n),
   .tx_en(uart_tx_en),
   .tx_data(uart_tx_data),
   .txd(uart0_txd)
);
```
第1行格式为：<子模块名称> {#(参数列表)} {<模块实例名字>} (

注：这里用{}表示可选内容。下同。

父模块代码中可以包含多个子模块的实例（比如我们可以设置多个串口），可以给每个子模块实例一个名字（目前用不上）。

当模块有参数且缺省值不合用时，需增加参数列表。

参数列表用“#(.<参数名字>(<参数值>){,.<参数名字>(<参数值>),...})”表示。注意有那个“.”号。

只有一个子模块且使用缺省参数值时（波特率设为115200时），第1行可简化为：
```
uart_tx(  
```

子模块的小括号里面是端口连接关系，格式为：.<子模块端口名称>(<父模块接线名称（wire或reg）>){,}

注意有那个“.”号。

不需要用上的端口可以不列（会有warning），但输入端口建议都列上并连上高电平或低电平。比如：
```
   .reset_n(1'b1),
```
表示不激活reset功能。

#### 声明参数

在uart_tx模块中，声明参数的语句是：
```
module uart_tx #(parameter BAUDRATE_DIVIDER=234) (
```

其格式为：module <模块名称> [#(参数列表)]

参数列表的格式为：parameter <参数名称>=<参数缺省值>{,<参数名称>=<参数缺省值>...}

其中parameter是关键字。注意没有“.”号。


#### 声明本地参数（常量名字）

我们可以给常数起一个名字，使用关键字localparam。

在uart_tx.v中给出一个例子：
```
localparam FRAME_LEN = 10;
reg [FRAME_LEN-1:0] send_pattern;
...
send_bitcnt <= FRAME_LEN;
```

#### 串口实例化代码解读

```
wire uart_tx_en = (counter2[16:0] == 17'b1); //trigger tx every 1s
wire [7:0] uart_tx_data = {~button, ~led};

uart_tx #(.BAUDRATE_DIVIDER(234)) u_uart0_tx(  
   .clk(sys_clk),
   .reset_n(reset_n),
   .tx_en(uart_tx_en),
   .tx_data(uart_tx_data),
   .txd(uart0_txd)
);
```
uart_tx模块有4个输入端口：
- clk接工作时钟，这里接27MHz时钟；
- reset_n接reset信号，低电平有效；
- tx_en是启动发送的脉冲信号，脉冲宽度应超过1个clk周期（clk的上升沿能捕获到），脉冲宽度应小于发送一帧数据的时间长度；
代码中：
```
wire uart_tx_en = (counter2[16:0] == 17'b1); //trigger tx every 1s
```
17'b1的值就是1。counter2的输入时钟是27MHz/206，经过17位D触发器的分频得到counter2[16]的周期是1秒。

每秒出现一次：(counter2[16:0] == 17'b1)脉冲，脉冲宽度是206/27(us)。
- tx_data是发送的数据，在tx_en为高电平的第1个clk上升沿锁存到发送缓冲区。
代码中tx_data接uart_tx_data：
```
wire [7:0] uart_tx_data = {~button, ~led};
```
改变uart_tx_data的内容可以收到不同的数据。

uart_tx模块有1个输出端口：
- txd：串行化的UART发送帧数据，接UART发送引脚。

波特率的设置是通过修改参数BAUDRATE_DIVIDER的值来设置的，这是在综合阶段固定了的，不能在烧写后运行过程中更改。（可更改的见原来的PicoSoC项目中的simpleuart.v源代码，设置了一个I/O寄存器来配置波特率）

BAUDRATE_DIVIDER的计算方式是clk/<波特率>，clk=27000000：
- 波特率设为115200时，BAUDRATE_DIVIDER=27000000/115200=234
- 波特率设为9600时，BAUDRATE_DIVIDER=27000000/9600=2812


#### 串口发送实现代码解读

uart_tx.v中，“reg [FRAME_LEN-1:0] send_pattern”保存待发送的物理帧数据。

当不使用奇偶校验时，一帧数据包含1比特开始（1'b0）、8比特用户数据、1比特结束（1'b1），共10比特。

send_pattern的内容在以下代码中设置：
```
always @(posedge clk) begin
...
    if (tx_en && !send_bitcnt) begin
        send_pattern <= {1'b1, tx_data[7:0], 1'b0};
        send_bitcnt <= FRAME_LEN;
        send_divcnt <= 0;
    end 
```
在clk的上升沿判断，若tx_en为1且当前没有正在发送数据（send_bitcnt==0），则设置send_pattern、重置send_bitcnt和send_divcnt。注意开始比特在bit0、结束比特在bit9。

send_divcnt（divide count）用来数clk的数目来决定什么时候发送下1位数据。它初始值为0，每来一个clk就加1，当大于等于BAUDRATE_DIVIDER时则发送下1位数据。
```
always @(posedge clk) begin
    send_divcnt <= send_divcnt + 1;
    
            else if ((send_divcnt >= BAUDRATE_DIVIDER) && send_bitcnt) begin
                send_pattern <= {1'b1, send_pattern[9:1]};
                send_bitcnt <= send_bitcnt - 1'b1;
                send_divcnt <= 0;
            end
```
发送下1位数据是通过右移实现：
```
                send_pattern <= {1'b1, send_pattern[9:1]};
```
注意txd端口接send_pattern的bit0：
```
    assign txd = send_pattern[0];
```
每发送1位则send_bitcnt减1，减到0时就不再触发移位。
```
                send_bitcnt <= send_bitcnt - 1'b1;
```
注意到移位时左边补1，则移位停止时，txd保持为高电平，直到下一次发送时再拉低。




