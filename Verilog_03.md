# Verilog入门（3）:时序逻辑

本节通过例子介绍简单时序逻辑电路的描述方法。

#### step1：D触发器的表示（1）

回顾数字电路教材，一个D触发器至少有一个输出端口Q、一个数据输入端口D、一个时钟输入端口clk，在clk的上升沿或下降沿将D的值送Q。

在Verilog中，我们可以用下面的代码来表示这样的一个D触发器：
```
reg q;
wire clk;
wire d;

always @(posedge clk) begin
    q <= d;
end
```

“reg”是关键字，reg是variable大类中常见的一种类型，代表数据存储元件的抽象。我们可以先粗糙地把它理解为1位寄存器。

reg类型的变量需要有配套的always结构来描述其输出数据的赋值行为。例子中是常见的一种。

“always @(<触发信号>) begin ...<执行动作>... end”是always结构的基本表示方式。

“posedge”是关键字（positive edge），表示上升沿。（下降沿是“negedge”）

“q <= d;”表示将d的值赋给q。对于reg类型的变量，赋值语句不需要用assign（assign用于net型的数据）。

“q <= d;”和“q = d;”同样是将d的值赋给q，但是它们是有区别的。“<=”表示这条语句和后面的语句是同时执行的，这个下来再解释。初学者一开始在always结构里面都用“<=”、记住always结构里面的语句都是同时执行的就好。


#### step2：D触发器的表示（2）

然后我们给这个D触发器增加一个异步reset输入端口reset_n，当reset_n为低时Q输出低电平。

在Verilog中，我们可以用下面的代码来表示这样的一个D触发器：
```
reg q;
wire clk;
wire reset_n;
wire d;

always @(posedge clk or negedge reset_n) begin
    if (~reset_n) begin
	    q <= 0;
	end
	else begin
	    q <= d;
	end
end
```
上面的例子中：
- 在触发事件表达式中新出现了关键字or，表示任意事件出现都能触发alway结构体的执行；
- 在结构体中新出现了关键字结构“if (<条件表达式> <任务语句集1> else <任务语句集2>)”。当“任务语句集”中出现多条语句时需加“begin ... end”，初学者可以先一律加上。


#### step3：通过按键toggle LED

组合逻辑帖子中的例子1是通过按键的状态来控制LED的亮灭，这里要求按一下按键，改变一次LED的亮灭状态。

例子1：通过按键控制LED的亮灭。

实现思路：
- 使用一个D触发器保存LED状态；
- 按键接D触发器的时钟输入端口，当按键按下时取反D触发器的输出状态；

参考代码：
```
module top
(
    input wire [1:0] button,
    output wire [5:0] led
);
reg q;
wire clk;
wire reset_n;

always @(negedge clk or negedge reset_n) begin
    if (~reset_n) begin
	    q <= 1;
	end
	else begin
	    q <= ~q;
	end
end

assign clk = button[0];
assign reset_n = button[1];

assign led[0] = q;

assign led[5:1] = {5{1'b1}};
endmodule
```

例子中有一个新的常数表达方式：“{5{1'b1}}”，表示5个1即5'b11111。

“{<重复次数>{<常数>}}”：将常数重复若干次，拼接起来。

<常数>可以是多比特，比如：“{4{3'b001}}”等于12b'001001001001。

大括号里面还可以拼接其它数据，比如：“assign led={4{1'b1},button};”等同于“assign led[5:2]=4'b1111; assign led[1:0]=button[1：0];”


#### step4：多位D触发器，计数器

“Vector”的概念同样可以用在reg数据类型上，用以表示多位D触发器。

“reg [5:0] q;”配合相应的always结构，表示一个6位的D触发器。

以设计一个计数器为例。

例子2：设计一个6位的计数器，每按一次按键1加1，用led显示结果，按键2重置计数器。

实现思路：
- 使用6位D触发器保存计数值；
- 计数值送LED显示；
- 按键1接计数器的时钟输入端口，当按键按下时计数值加1；
- 按键2接计数器的reset输入端口；


参考代码：
```
module top
(
    input wire [1:0] button,
    output wire [5:0] led
);
reg [5:0] q;
wire clk;
wire reset_n;

always @(negedge clk or negedge reset_n) begin
    if (~reset_n) begin
	    q <= 0;
	end
	else begin
	    q <= q + 1'b1;
	end
end

assign clk = button[0];
assign reset_n = button[1];

assign led = ~q;

endmodule
```

说明：
- 多位D触发器：“reg [5:0] q;”
- 加一：“q <= q + 1'b1;”，也可以写为“q <= q + 1;”，但会多一个truncate warning。
- 显示：“assign led = ~q; ”，取反是因为led是低电平亮，而我们习惯led亮时表示数据1。
- 测试时，有时按键按一次会加超过1的数，是因为按键有抖动，现在的这个电路没有加去抖。


#### step5：定时器，分频器

上例中的计数器的时钟输入端口接周期时钟信号，就是一个定时器，第n位D触发器的输出引出来，就是输入时钟信号的2^(n+1)次分频信号（q[0]是时钟的二分频信号、q[1]是时钟的4分频信号、q[9]是时钟的1024分频信号）。

模块的晶振是27MHz，接在芯片的52脚。

例子3：设计一个6位的计数器，每秒加1，用led显示结果，按键2重置计数器。

这里的实现重点是产生周期为1秒的时钟信号。

实现思路：
- 增加sys_clk输入端口，接芯片52脚；
- 27000000=206 * 1024 * 128；
- 将输入的27MHz信号（周期设为T0）作为计数器1的时钟输入，每103个时钟toggle输出，产生周期为206个T0的时钟信号T1；
- 用计数器2将信号T1进行2^17分频，产生周期为1秒的时钟信号T2；
- 用T2驱动计数器3，显示计数器3的值；
（事实上，不需要计数器3，用计数器实现分频的话，扩展计数器2的位数，显示计数器2的bit22-17的值即可）

参考代码：
```
module top
(
    input wire [1:0] button,
    input wire sys_clk,
    output wire [5:0] led
);

reg [6:0] counter1;
wire reset_n;
reg clk_206T0;

always @(posedge sys_clk or negedge reset_n) begin
    if (~reset_n) begin
	    counter1 <= 0;
        clk_206T0 <=0;
	end
	else begin
        if (counter1 >= 102) begin
            counter1 <= 0;
            clk_206T0 <= ~clk_206T0;
        end
        else begin
	        counter1 <= counter1 + 1'b1;
        end    
	end
end

reg [22:0] counter2;

always @(posedge clk_206T0 or negedge reset_n) begin
    if (~reset_n) begin
	    counter2 <= 0;
	end
	else begin
	    counter2 <= counter2 + 1'b1;
	end
end

assign reset_n = button[1];

assign led = ~counter2[22:17];

endmodule
```
tangnano9k.cst：增加sys_clk定义。
```
IO_LOC "led[5]" 16;
IO_PORT "led[5]" PULL_MODE=UP DRIVE=8;
IO_LOC "led[4]" 15;
IO_PORT "led[4]" PULL_MODE=UP DRIVE=8;
IO_LOC "led[3]" 14;
IO_PORT "led[3]" PULL_MODE=UP DRIVE=8;
IO_LOC "led[2]" 13;
IO_PORT "led[2]" PULL_MODE=UP DRIVE=8;
IO_LOC "led[1]" 11;
IO_PORT "led[1]" PULL_MODE=UP DRIVE=8;
IO_LOC "led[0]" 10;
IO_PORT "led[0]" PULL_MODE=UP DRIVE=8;
IO_LOC "button[1]" 3;
IO_PORT "button[1]" PULL_MODE=UP;
IO_LOC "button[0]" 4;
IO_PORT "button[0]" PULL_MODE=UP;
IO_LOC "sys_clk" 52;
IO_PORT "sys_clk" IO_TYPE=LVCMOS33 PULL_MODE=UP;
```

运用上面例子的思路，我们可以生成任意周期为2n*T0（T0为晶振信号周期，n为正整数）的方波信号。

下面我们来生成PWM信号。

#### step6：生成PWM信号

例子4：设计一个呼吸灯，LED由暗变亮，循环。

这里的实现重点是产生PWM信号。

实现思路：
- 在计数器代码中，当计数值小于某个值k时输出高电平，否则输出低电平；
- 该比较值k乘以计数器时钟周期T就是脉冲的宽度；
- 计数器的最大值加一乘以计数器时钟周期T就是PWM信号的周期；
- 用另外一个计数器产生比较值k就能产生呼吸灯的效果；

参考代码：
```

//use 1s counter to generate the pulse width compare value,
//changed from 0 to 255 with 2s.

reg [6:0] counter1;
wire reset_n;
reg clk_206T0;

always @(posedge sys_clk or negedge reset_n) begin
    if (~reset_n) begin
	    counter1 <= 0;
        clk_206T0 <=0;
	end
	else begin
        if (counter1 >= 102) begin
            counter1 <= 0;
            clk_206T0 <= ~clk_206T0;
        end
        else begin
	        counter1 <= counter1 + 1'b1;
        end    
	end
end

reg [22:0] counter2;

always @(posedge clk_206T0 or negedge reset_n) begin
    if (~reset_n) begin
	    counter2 <= 0;
	end
	else begin
	    counter2 <= counter2 + 1'b1;
	end
end

wire [7:0] pulse_width = counter2[17:10];

//pwm generater
reg [7:0] pwm_counter;
reg pwm_output;

always @(posedge clk_206T0 or negedge reset_n) begin
    if (~reset_n) begin
	    pwm_counter <= 0;
        pwm_output <= 0;
	end
	else begin
	    pwm_counter <= pwm_counter + 1'b1;
        if (pwm_counter < pulse_width) begin
            pwm_output <= 1;
        end
        else begin
            pwm_output <= 0;
        end
	end
end

//assoiciate pins
assign reset_n = button[1];
assign led[0] = ~counter2[17];
assign led[5:1] = {5{~pwm_output}};

```

这里PWM信号的生成代码其实很简单：
```
        if (pwm_counter < pulse_width) begin
            pwm_output <= 1;
        end
        else begin
            pwm_output <= 0;
        end
```
更多的代码是用于生成变化的pulse_width。

通过消化上面几个例子，大家再对照单片机芯片手册（ATMega、STM32）中的定时器章节，关于定时器的工作模式和输出比较功能（Output Compare）的模式，就会发现，实现一个定时器并不困难。

大家可以尝试设计以下模式的定时器和PWM信号：
- 定时器模式：UP、DOWN、UP-DOWN；
- PWM：单边、中心对称；
（例程实现的是UP+单边PWM）

#### step7：循环移位，跑马灯

例子5：设计一个跑马灯。

跑马灯的意思就是LED的显示模式循环左移或右移。

实现的核心代码：（假设6位led）
- 左移：led_value <= {led_value[4:0], led_value[5]}；
- 右移：led_value <= {led_value[0], led_value[5:1]}；

自行消化。

参考代码：（在之前的产生1s时钟信号的代码基础上增加）
```
wire clk_1s = counter2[16];

//round shift
reg [5:0] led_value;

always @(posedge clk_1s or negedge reset_n) begin
    if (~reset_n) begin
	    led_value <= 6'b000011;  //initial pattern of leds
	end
	else begin
	    led_value <= {led_value[4:0], led_value[5]};
	end
end
```

自行尝试：
- 改变led pattern；
- 通过按键切换左移右移；
- 通过按键加减移动速度；

#### 小结

本节简单介绍了如下Verilog关键字和概念：
- reg：声明变量数据类型，与always结构一起用于D触发器的表示；
- always结构：“always @(<触发信号>) begin ...<执行动作>... end”，与reg一起用于D触发器的表示；
- posedge、negedge：用于always结构中表示触发信号的触发沿；
- or：用于always结构中表示多个触发信号；
- <=：always结构中的对reg变量的赋值语句；
- if结构：“if (<条件表达式> <任务语句集1> else <任务语句集2>)”，表示电路块的选择通过；
- “{<重复次数>{<常数>}}”：将常数重复若干次，拼接起来；
- reg vector；

本节介绍了如下时序逻辑电路的实现方式：
- 计数器；
- 定时器；
- 分频器；
- PWM输出；
- 呼吸灯；
- 跑马灯；

（详细代码见：02_verilog_step03工程源码，里面包含多个例子，注释掉其它例子来尝试某个例子）

