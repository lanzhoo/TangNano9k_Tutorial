# 简单CPU原型设计（5）：UART TX

本节增加外设UART发送模块。


## 概念

- 软件如何控制硬件（芯片外围电路）？

- 外设设计的大致思路

见前贴。



#### 串口（UART）外设

基本所有的单片机/嵌入式芯片都有串口（UART）这种外设。

UART中的A是异步的意思，表示通信接口中没有时钟信号线用作同步，节点根据预先协商好的波特率对接收信号进行采样判决（比特同步），而帧同步则通过UART帧格式中的起始码和结束码实现。

因此UART只需要三根线：TX、RX、GND，不算GND的话就是两根线。

UART的主要参数包括：
- 波特率：每秒发送多少个符号；
- 数据位数：一帧包含多少位数据，按规范是5到9，一般都用8；
- 停止位数：一般是1位；
- 校验位：奇偶校验，可以是奇校验、偶校验或无校验；
- 硬件流量控制：是否使用额外的两根线进行握手控制；

TTL电平的UART是指在RX、TX信号线上用GND表示0、VCC表示1。RS232是进行了电平转换（用+12V表示0，-12V表示1）的UART。

没有传输的时候，TX信号线送1（也叫空闲位）。起始位是送1个符号宽度的0。然后是若干位（比如8位）的数据（低位比特先发送）。然后是可选的校验位，然后是停止位（1个符号宽度的1）。然后再接若干位的空闲位（至少1位）。



## 例程设计

### 硬件实现

复制工程03_tinycore_step04到03_tinycore_step05，修改工程文件名称。

为了简化实现，UART实现采用如下固定参数：
- 波特率为115200；
- 无奇偶校验；
- 数据位8位，停止位1位；
- 无硬件流控；


#### 增加peripheral_uart模块

为体现工程的层次化结构，peripheral模块包含peripheral_uart，peripheral_uart包含具体实现模块uart_tx。

peripheral_uart模块的端口包含从core来的总线端口和从引脚来的uart端口。

peripheral_uart.v：
```
module peripheral_uart (
    input wire clk,
    input wire reset_n,
    input wire rxd,
    output wire txd,
    input wire ce, //chip enable
    input wire wre, //write enable
    input wire [1:0] addr, //address bus
    input wire [31:0] data_in,
    output wire [31:0] data_out
);
```

uart发送的实现参照“Verilog语句介绍（5）”，引用uart_tx模块实现。

这里主要增加与core接口的I/O寄存器及相关控制逻辑。

假设外设地址空间的0x004-0x007单元分配给UART外设，每个地址单元32位数据。

外设中先设计一个发送数据寄存器，地址为0x004。当core写该地址（0x00000810：（0x00000800+0x004<<2））时，把发送的数据锁存，然后送uart_tx模块并触发发送。

锁存数据代码：
```
    reg [31:0] data_tx_reg;

    wire uart_tx_en = ce & wre & (addr==2'b00);

    //write data_tx_reg register
    always @(posedge clk) begin
        if (~reset_n) begin
            data_tx_reg <= 32'b0;
        end
        else if (uart_tx_en) begin
            data_tx_reg <= data_in;
        end
    end    
```

由于uart_tx需要提供一个clk周期宽度的脉冲信号作为发送触发信号。需要通过一个移位寄存器来判断core写入信号的上升沿，然后产生一个clk周期宽度的脉冲：
```
    //generate tx pulse
    reg [1:0] shift_reg;
    always @(posedge clk) begin
        if (~reset_n) begin
            shift_reg <= 2'b0;
        end
        else begin
            shift_reg <= {shift_reg[0], uart_tx_en};
        end
    end    
    wire uart_tx_en_pulse = (shift_reg[1] == 0) & (shift_reg[0] == 1);

```


由uart_tx负责构造数据帧并逐步移位发送。（uart_tx的核心是一个移位寄存器）

```
    //Baudrate=115200; DIVIDER=27,000,000/115200=234.375
    uart_tx #(.BAUDRATE_DIVIDER(234)) u_uart0_tx(  
       .clk(clk),  //27MHz
       .reset_n(reset_n),
       .tx_en(uart_tx_en_pulse),
       .tx_data(data_tx_reg[7:0]),
       .txd(txd)
    );
```
（复制02_verilog_step05\src下的uart_tx.v到本工程src目录。并把该文件加入本工程：点选“Verilog Files”右键“Add Files”）

#### peripheral.v


在peripheral模块中，增加来自引脚的uart端口：
```
module peripheral (
    //from top
    input wire clk,
    input wire reset_n,
    input wire button,
    output wire [5:0] led,
    input wire uart_rxd,
    output wire uart_txd,    
    //from core
    input wire ce, //chip enable
    input wire wre, //write enable
    input wire [8:0] addr, //address bus
    input wire [31:0] data_in,
    output wire [31:0] data_out
);
```

在peripheral模块中，增加对uart模块的引用：
```
    //uart
    //
    wire uart_ce = (addr[8:2] == 7'b0000001) ? 1'b1 : 1'b0;

    wire [31:0] uart_data_out;

    peripheral_uart m_uart0(
        .clk(clk),
        .reset_n(reset_n),
        .rxd(rxd),
        .txd(txd),
        .ce(uart_ce),
        .wre(wre),
        .addr(addr[1:0]),
        .data_in(data_in),
        .data_out(uart_data_out)
    );
```

因为gpio的data_out和uart的data_out都要接peripheral的data_out，会引起冲突。需要加个mux。

修改对gpio模块的引用：
```
    wire [31:0] gpio_data_out;
    gpio m_gpio(
...
        .data_out(gpio_data_out)

    );
```
增加判断选择：
```

```


其中假设外设地址空间的0x004-0x007单元分配给UART，每个地址单元32位数据。

则uart模块的ce信号为：
```
    wire uart_ce = (ce & (addr[8:2] == 7'b0000001)) ? 1'b1 : 1'b0;
```


#### top.v


增加来自引脚的uart端口：

```
module top
(
    input wire [1:0] button,
    input wire sys_clk,
    output wire [5:0] led,
    input wire uart0_rxd,
    output wire uart0_txd
);
```

引用peripheral模块时传入uart端口：
```
    //peripherals
    peripheral m_peripheral(
        .clk(sys_clk),
        .reset_n(reset_n),
        .button(button[0]),
        .led(led),
        .rxd(uart0_rxd),
        .txd(uart0_txd),
        //
        .ce(peripheral_ce),
        .wre(peripheral_wre),
        .addr(peripheral_addr),
        .data_in(peripheral_data_out),
        .data_out(peripheral_data_in)
    );
```


#### tangnano9k.cst

增加uart rxd和txd引脚：

```
IO_LOC "uart0_txd" 17;
IO_PORT "uart0_txd" IO_TYPE=LVCMOS33 PULL_MODE=UP DRIVE=8;
IO_LOC "uart0_rxd" 18;
IO_PORT "uart0_rxd" IO_TYPE=LVCMOS33 PULL_MODE=UP;

```






### 软件实现

在上文的程序基础上修改，把“将x16的值保存到GPIO模块的数据输出I/O寄存器”改为“将x16的值保存到UART模块的发送数据寄存器”：
- 循环：
- 从存储单元m0读数据到x15；
- x15+1送x16；
- 将x16的值保存到存储单元m0；
- 将x16的值保存到UART模块的发送数据寄存器；



#### 程序代码

程序代码在rom_data.mi中，5条指令。须重新生成gowin_prom。

```
#File_format=Hex
#Address_depth=8
#Data_width=32
00002783
00178813
01002023
41002823
ff1ff06f
00000000
00000000
00000000
```

或者直接修改gowin_prom.v：
```
defparam prom_inst_0.INIT_RAM_00 = 256'h000000000000000000000000ff1ff06f41002823010020230017881300002783;
```

#### 指令解读
汇编程序为：
LOOP: LW x15, 0(x0)
      ADDI x16, x15, #1
      SW x16, 0(x0)
      SW x16, 1040(x0)      
      J LOOP

其中前3条和最后1条与前一节的相同，第4条汇编语言的区别是地址不一样（由0x00000800改为0x00000810）。      
      
第1条指令：从RAM地址0处取值送x15。LW格式为<12b imm=0><5b rs1=0> func3=010 <5b rd=01111> opcode=0000011 

二进制值为：0b0000,0000,0000,0000,0010,0111,1000,0011。即0x00002783

对应汇编指令为：LW x15, 0(x0)

第2条指令：x15加1送x16。ADDI格式为<12b imm=1><5b rs1=01111> func3=000 <5b rd=10000> opcode=0010011

二进制值是：0b0000,0000,0001,0111,1000,1000,0001,0011。即0x00178813

对应汇编指令为：ADDI x16, x15, #1

第3条指令：将x16的值送RAM地址0单元。SW格式为<7b imm=0><5b rs2=10000><5b rs1=0> func3=010 <5b imm=0> opcode=0100011 

二进制值为：0b0000,0001,0000,0000,0010,0000,0010,0011。即0x01002023

对应汇编指令为：SW x16, 0(x0)

第4条指令：将x16的值送外设地址4单元即0x0410处。SW格式为<7b imm=0100000><5b rs2=10000><5b rs1=0> func3=010 <5b imm=10000> opcode=0100011 

二进制值为：0b0100,0001,0000,0000,0010,1000,0010,0011。即0x41002823

对应汇编指令为：SW x16, 1040(x0)

第5条指令：跳转到4条指令前即16字节。JAL格式为<imm[20|10:1|11|19:12]><5b rd=0> opcode=1101111

-16对应1..11,0000。注意bit0不在指令里。所以<imm[20|10:1|11|19:12]>值为1|1111111000|1|11111111

二进制值为：0b1111,1111,0001,1111,1111,0000,0110,1111。即0xff1ff06f

对应汇编指令为：JAL x0, -16

大概等效c语言程序为：
```
#define MEM_UNIT_0 *((volatile unsigned int *)0x00000000)
#define UART_TXR *((volatile unsigned int *)0x00000410)

void main(void) {
  int i, j;
  while(1) {
    i = MEM_UNIT_0;
    j=i+1;
    MEM_UNIT_0 = j;
    UART_TXR = j;
  }
}
```

#### 运行效果


可以观察到led约每5秒加1。


