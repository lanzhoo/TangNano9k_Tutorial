# Verilog入门（4）:我对Verilog规范的理解

Verilog相关书籍和文章的知识源自哪里？规范文档。要想准确、详细理解Verilog语言，在阅读相关书籍和文章之外，建议读一下规范。

（为方便大家学习，相关规范上传在https://gitee.com/lanzhoo/specs_and_datasheets）


#### Verilog规范的版本

Verilog语言的规范文档为IEEE 1364，第1版是1995年发布，为IEEE 1364-1995，第2版是IEEE 1364-2001。

然后IEEE在2005年发布了SystemVerilog的第1个版本，为：IEEE 1800-1995，对应的修订了Verilog，发布了IEEE 1364-2005。

之后就基本上是修订SystemVerilog的版本，包括IEEE 1800-2012和IEEE 1800-2017。

本系列基于IEEE 1364-2001规范介绍Verilog语言。

#### 为什么选择Verilog而不是SystemVerilog或其它高级语言？

粗糙地类比一下：Verilog相当于c或者汇编语言，SystemVerilog相当于C++，其它高级语言相当于Python。

我们的目标是透彻理解怎样用硬件描述语言去描述数字电路，重点在透彻理解，而不在迅速设计一个应用。因此选择的工具不是越高级越易用就越好，而是尽量少一些黑箱，能上下贯通，理解明白。

Verilog首先是写给综合器（Synthesis）看的，综合器将Verilog代码转换为电路，用网表（net-list）表示，这个就相当于PCB设计中的原理图。然后FPGA厂家的布线工具（Place and Route）根据FPGA芯片提供的基本元器件实现该电路，就相当于PCB设计中的PCB版图。

（问题是Verilog语言和SystemVerilog语言中还有许多内容是写给仿真器看的，造成初学者觉得要学的东西太多太乱。）

现在已有许多高级工具和高级语言（比如Matlab、Vivado HLS（C/C++/SystemC）、Python、某些图形化编程工具等）可以用计算机编程语言或者其它支持面向对象的硬件描述语言来描述电路，然后转换为Verilog语言，再用Verilog综合器转换为电路（也有直接生成电路的）。

我们先从Verilog入门，以后再学其它高级工具，就相当于先学c语言掌握原理，以后再学Python等语言用于快速设计应用（用于快速设计应用的工具、语言、平台、中间件很多，根据需要选择）。

#### Verilog规范中的哪些内容可以先不学？

Verilog_IEEE1364-2001规范有27章，对于初学者来说哪些内容先学，哪些可以不学呢？

首先，要明白Verilog规范中的术语和概念除了面向电路描述外，还有许多是面向电路仿真、是写给仿真器看的，而我们现在是用实际FPGA芯片进行调试，因此面向仿真的可以先不学。

其次，Verilog规范提供了对电路描述的多层次抽象，要表达同样的电路可以用多种形式描述。那么，我们就先学一种格式，其它的先放过。那么我们要学的东西就少很多了。

1、去掉面向电路仿真的章节
- “5. Scheduling semantics”；
- “10. Tasks and functions”；
- “17. System tasks and functions”；
- “18. Value change dump (VCD) files”；
- 第20到27章：高级语言接口，用多种高级语言与仿真平台交互；

2、去掉用门电路级别原语来描述电路的章节
- “7. Gate and switch level modeling”：基于门和开关层次建模；
- “8. User-defined primitives (UDPs)”：用户定义原语；

3、去掉初学时用不上的章节（大部分跟时序约束有关）
- “6. Assignments”；
- “11. Disabling of named blocks and tasks”；
- “13. Configuring the contents of a design”；
- “14. Specify blocks”；
- “15. Timing checks”；
- “16. Backannotation using the Standard Delay Format (SDF)”；
- “19. Compiler directives”；

也就是说，我们学的内容主要在章节1-4、9、12。


#### 具体内容和学习顺序

注：直接阅读规范文档虽然很有用，但对不熟悉规范语法描述语言（类Pascal）的初学者来说难度较大，可以简单阅读一下然后对照Verilog相关书籍来阅读。

1、module和层次化结构

理解层次化结构：模块包含模块和电路。（就像芯片手册中的方框图）

这个是在第12章“12. Hierarchical structures”，一开始不需要细看，逐步了解以下内容：
- module和endmodule关键字；
- 端口列表；
- 怎样包含/使用子模块（子模块的例化）；
- 怎样定义和使用可变参数（综合阶段例化子模块时可改变的参数）；


2、数据类型

第3章：“3. Data types”。
- “3.1 Value set”：
一条线上的电平（常量或变量）除了“0”和“1”表示低电平和高电平外，还有“x”表示未知、“z”表示高阻；
- “3.2 Nets and variables”：
两种数据类型：net和variable。“There are two main groups of data types: the variable data types and the net data types.”。
- “3.2.1 Net declarations”：
一开始会用wire就可以了；
- “3.2.2 Variable declarations”：
一开始会用reg就可以了；
- “3.3 Vectors”：多条线的表示，看一下；
- “3.4 Strengths”：信号强度，先不用管；
- “3.5 Implicit declarations”、“3.6 Net initialization”：先不用管；
- “3.7 Net types”：会wire就可以了；
- “3.8 regs”：看一下，关联6.2和9.2；
- “3.9 Integers, reals, times, and realtimes”：其它变量类型，遇到时可以看一下；
- “3.10 Arrays”：数组，多束线的表示方法，看一下；
- “3.11 Parameters”：参数，用来表示常量，看一下关键字localparam和parameter；
- “3.12 Name spaces”：先不用管；

3、表达式

第4章：“4. Expressions”。“This clause describes the operators and operands available in the Verilog HDL and how to use them to form expressions.”

要学习有哪些操作符可以用、分别是什么功能，可以在这章里面找（4.1）。具体小节不列出来了。可以参考“Table 9—Operators in the Verilog HDL”一览。


4、always结构体

第9章：“9. Behavioral modeling”。

可以细看的包括：
- “9.1 Behavioral model overview”；
- “9.4 Conditional statement”：if-else结构；
- “9.5 Case statement”：Case结构，可以用于状态机；

5、循环生成类似代码

对于电路中包含大量的类似实例，可以通过generate - endgenerate结构（12.1.3 Generated instantiation）进行生成。



