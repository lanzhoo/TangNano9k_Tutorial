# 高云云源软件（Gowin）安装配置简介

#### 过程

1、上官网下载教育版Gowin程序(V1.9.8.03 Education)

地址：http://www.gowinsemi.com.cn/faq.aspx

教育版不需要license，只能用于教育研究等非盈利用途。

注：2022年9月最新版本是“云源软件 for win 教育版(V1.9.8.07 Education)”，但是该版本的programmer不认得TangNano9k模块，所以下载V1.9.8.03版本。

2、按指引安装

直接next就可以。

缺省会安装好IDE和烧写器（Programmer）程序以及USB驱动。（提示安装USB驱动时两个勾都不要动）

“C:\Gowin\Gowin_V1.9.8.03_Education\IDE\doc\CN”目录下有IDE软件使用指南。芯片手册需要从高云官网下载。

3、下载例程

下载本仓库例程01_led或者该模块在github上的LED例程（https://github.com/sipeed/TangNano-9K-example）。

以下按01_led介绍。


4、运行软件

运行gowin软件，打开工程（Open Project）01_led\led.gprj

![打开工程](images/open_project.png)

5、执行综合

点击上图中鼠标所在位置上的“Run All”图标。

![执行综合](images/process.png)

点击左边工程文件列表框下面的“Process”标签，可以切换到综合/布线过程视图，观察综合过程。在下面的Console窗口，切换Console/Message可以观察处理过程输出的Warning和error消息。在Console窗口点右键可以清除消息。（注：重复“Run All”不会自动清除消息。）


6、插入模块

查看设备管理器，在“端口（COM和LPT）”可以看到USB串口。

7、烧写

执行菜单上的“Tools/Programmer”，启动烧写器软件。

第一次使用时比较麻烦，启动后点击图标栏最左边的“Scan”图标，提示框选择“GW1NR”：

![scan](images/programmer_01.png)

双击Operation下“Read Device Code”，修改操作为“SRAM Program”：

![SRAM Program](images/programmer_02.png)

确定后提示选择烧写文件，要烧写的文件时在工程的“impl\pnr”目录下的.fs文件。点save。

然后点击图标栏最右边的“Program”图标：

![Program](images/programmer_03.png)

等待烧写完成。

8、观察效果

这个例子是亮两灯的跑马灯，同时在串口输出led对应数值，大概500ms变化一次。串口波特率为115200，无校验，勾选十六进制显示。

![模块](images/module.png)
![串口显示](images/serial.png)

#### 软件常用操作

1、IDE

（1）左边窗口切换Design和Process。Design下列出工程的文档结构，双击文件名查看文件内容。Process下查看综合/布线过程是否结束，布线（Place&Route）结束后才可以烧写。

（2）下面窗口切换Console和Message。Message窗口可以分类查看Info/Warning/Error，双击Warning/Error消息可以定位到对应位置。Console窗口同样可以观察综合/布线过程是否结束、以及Info/Warning/Error消息。

2、Programmer

（1）修改代码后再烧写，只需点击“Program”图标。

（2）IDE更换工程后，Programmer须手动更换烧写文件，即两者没有联动。（如果从Process页面双击“Program Device”启动Programmer则可以自动更换烧写文件。）

（3）烧写到SRAM的bitstream掉电后就失效，如果想保留则在配置界面改为烧写到内部flash。（注：平时一般都烧到SRAM）

![flash](images/programmer_04.png)