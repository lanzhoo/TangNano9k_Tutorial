/*+***********************************************************************************
 Filename: Blink03.ino
 Description: led and uart_tx demo, co-operated with Tangnano9k fpga based mcu project.
    Demo using hal layer function. 

 Modification:
   2022.11.28 Creation   H.Zheng

Copyright (C) 2022  Zheng Hui (hzheng@gzhu.edu.cn)

License: MulanPSL-2.0

***********************************************************************************-*/

#include "halTangnanoTiny.h"

unsigned int gCount;

void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:
  gCount++;
  halSetLed(gCount);
  halUartSend(gCount);

  halDelay(10);
}
