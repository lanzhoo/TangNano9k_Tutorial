/*+***********************************
Filename: main.c
Description: simple main.
Modification:
2022.11.14 hzheng creation
************************************-*/
#include "Arduino.h"

extern void donothing();

int main(){          
    donothing(); //for linking the vectors.S

    setup();

    while (1) {
        loop();
    } 
        
    return 0;
}
