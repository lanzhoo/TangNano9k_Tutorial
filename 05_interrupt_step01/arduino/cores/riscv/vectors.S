/*+***********************************
Filename: vectors.S
Description: interrupt vectors and ISR entry.
Modification:
2022.12.12 hzheng creation
************************************-*/

# macros copied from esp32.vectors
    .equ SAVE_REGS, 8
    .equ CONTEXT_SIZE, (SAVE_REGS * 4)

.macro save_general_regs cxt_size=CONTEXT_SIZE
    addi sp, sp, -\cxt_size
    sw   ra, 0(sp)
    sw   a0, 4(sp)
    sw   a1, 8(sp)
    sw   a2, 12(sp)
    sw   a3, 16(sp)
    sw   a4, 20(sp)
    sw   a5, 24(sp)
    sw   a6, 28(sp)
.endm

.macro restore_general_regs cxt_size=CONTEXT_SIZE
    lw   ra, 0(sp)
    lw   a0, 4(sp)
    lw   a1, 8(sp)
    lw   a2, 12(sp)
    lw   a3, 16(sp)
    lw   a4, 20(sp)
    lw   a5, 24(sp)
    lw   a6, 28(sp)
    addi sp,sp, \cxt_size
.endm

### my vectors entry

    .global _global_interrupt_handler

    .section .exception_vectors.text,"a"
###    .section .exception_vectors
    
    .balign 0x100
    .global _vector_table
    .type _vector_table, @function
_vector_table:
    .rept (16)
    j _interrupt_handler
    .endr
    
    .global _interrupt_handler
    .type _interrupt_handler, @function
_interrupt_handler:
    save_general_regs
    jal _global_interrupt_handler
    restore_general_regs
    mret

#let main() call this to link this file    
    .global donothing
    .type donothing, @function
donothing:
    ret 

    
