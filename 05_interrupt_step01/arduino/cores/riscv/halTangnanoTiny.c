/*+***********************************
Filename: halTangnanoTiny.c
Description: hal layer function for tiny mcu on Tangnano9k.
Modification:
2022.11.28 hzheng creation
************************************-*/

#include "halTangnanoTiny.h"

#define GPIO_ODR      *((volatile unsigned int *)0x00000000)
#define UART0_TX_Send *((volatile unsigned int *)0x00000010)


void halSetLed(volatile unsigned int vData){          
  GPIO_ODR = vData;
}

void halUartSend(volatile unsigned int vData){          
  UART0_TX_Send = vData;
}

void halDelay(unsigned int vData){
  if (vData == 0)
    return;
  unsigned int i;
  for (i=0;i<vData;i++) {
    asm volatile(
      "nop;"
    );
  }
}