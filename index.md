# 基于TangNano9k开展RISC-V SoC设计入门

####  专题目录

1、Gowin环境搭建步骤：[setup.md](setup.md)；工程：01_led

2、Verilog语句介绍（1）: 工程目录及最基本语句：[Verilog_01.md](Verilog_01.md)；工程：02_verilog_step01

3、Verilog语句介绍（2）: 组合逻辑电路：[Verilog_02.md](Verilog_02.md)；工程：02_verilog_step02

4、Verilog语句介绍（3）: 时序逻辑电路：[Verilog_03.md](Verilog_03.md)；工程：02_verilog_step03

5、Verilog语句介绍（4）:我对Verilog规范的理解：[Verilog_04.md](Verilog_04.md)；

6、Verilog语句介绍（5）: 模块的引用、uart_tx代码解读：[Verilog_05.md](Verilog_05.md)；工程：02_verilog_step04

7、Verilog语句介绍（6）: CASE语句和有限状态机（FSM）：[Verilog_06.md](Verilog_06.md)；工程：02_verilog_step05

8、Verilog语句介绍（7）: 自动生成类似代码段：generate和for：[Verilog_07.md](Verilog_07.md)；工程：02_verilog_step06

9、Verilog语句介绍（8）: 小结：[Verilog_08.md](Verilog_08.md)；

10、简单CPU原型设计（1）: 微机原理回顾；厂家IP模块引用（PLL和ROM）：[TinyCore_01.md](TinyCore_01.md)；工程：03_tinycore_step01

11、简单CPU原型设计（2）: 译码与执行：加法与跳转：[TinyCore_02.md](TinyCore_02.md)；工程：03_tinycore_step02

12、简单CPU原型设计（3）: RAM、LOAD/STORE：[TinyCore_03.md](TinyCore_03.md)；工程：03_tinycore_step03

13、简单CPU原型设计（4）: 外设、GPIO：[TinyCore_04.md](TinyCore_04.md)；工程：03_tinycore_step04

14、简单CPU原型设计（5）: 外设UART_TX：[TinyCore_05.md](TinyCore_05.md)；工程：03_tinycore_step05

15、基于Arduino IDE进行开发（1）: 开发环境搭建：[Arduino_01.md](Arduino_01.md)；关联仓库：https://gitee.com/lanzhoo/arduino_riscvgcc_tools

16、基于Arduino IDE进行开发（2）: Hardware Loader设计：[Arduino_02.md](Arduino_02.md)；工程：04_arduino_step01

17、基于Arduino IDE进行开发（3）: 增加lui、jalr、bne等指令：[Arduino_03.md](Arduino_03.md)；工程：04_arduino_step02

18、基于Arduino IDE进行开发（4）: c中的HAL层led和uart tx函数：[Arduino_04.md](Arduino_04.md)；工程：04_arduino_step03；Arduino代码：04_arduino_step03/arduino

19、中断（1）: 原理及RISC-V规范：[Interrupt_01.md](Interrupt_01.md)；

20、中断（2）:简单按键中断实现：[Interrupt_02.md](Interrupt_02.md)；工程：05_interrupt_step01

21、MCU（1）:一个较完整MCU例子，未调通中断：基于tinyriscv core + JTAG + MounRiver_Studio：[MCU_tinyriscv_01.md](MCU_tinyriscv_01.md)；工程：06_tinyriscv_01_mcu01；c工程代码：06_tinyriscv_01_mcu01/c_mrs_prj

22、MCU（2）:另一个完整MCU例子，支持中断：基于SparrowRV core + JTAG + MounRiver_Studio：[MCU02_sparrowrvcore_01.md](MCU02_sparrowrvcore_01.md)；工程：06_mcu02_sparrowrv_01；c工程代码：06_mcu02_sparrowrv_01/c_mrs_prj




#### 参考资源

胡振波先生的开源蜂鸟E203源码及书籍“手把手教你设计CPU -- RISC-V处理器”。

顾长怡先生的书籍“基于FPGA与RISC-V的嵌入式系统设计”及其配套开源代码。

David A. Patternson教授的教材“计算机组成与设计 硬件/软件接口 RISC-V版”。

Tang Nano9k网上资料位置： 
* https://dl.sipeed.com/shareURL/TANG/Nano%209K
* https://en.wiki.sipeed.com/hardware/zh/tang/Tang-Nano-9K/Nano-9K.html
* https://github.com/sipeed/TangNano-9K-example

IDE工具: 云源软件-教育版: http://www.gowinsemi.com.cn/faq.aspx

