module top
(
    input wire [1:0] button,
    input wire sys_clk,
    output wire [5:0] led
);


    reg [29:0] counter;   
    wire reset_n = button[1];

    //the first D flip-flop
    always@(posedge sys_clk or negedge reset_n) begin
        if (~reset_n) begin
            counter[0]<=0;
        end
        else begin
            counter[0] <= ~counter[0];
        end
    end
    //the other D flip-flops
    genvar i;
    generate
        for (i=1; i<30; i=i+1) begin : counter_regs
            always@(negedge counter[i-1] or negedge reset_n) begin
                if (~reset_n) begin
                    counter[i]<=0;
                end
                else begin
                    counter[i] <= ~counter[i];
                end
            end
        end
    endgenerate

    //
    assign led = ~counter[29:24];

endmodule