# TangNano9k_Tutorial

#### 介绍
基于TangNano9k模块（Sipeed模块/高云FPGA芯片）介绍Verilog、基于FPGA设计RISC-V CPU入门。

#### 参考资源：

胡振波先生的开源蜂鸟E203源码及书籍“手把手教你设计CPU -- RISC-V处理器”。

顾长怡先生的书籍“基于FPGA与RISC-V的嵌入式系统设计”及其配套开源代码。

David A. Patternson教授的教材“计算机组成与设计 硬件/软件接口 RISC-V版”。

Tang Nano9k网上资料位置： 
* https://dl.sipeed.com/shareURL/TANG/Nano%209K
* https://en.wiki.sipeed.com/hardware/zh/tang/Tang-Nano-9K/Nano-9K.html
* https://github.com/sipeed/TangNano-9K-example

IDE工具: 云源软件-教育版: http://www.gowinsemi.com.cn/faq.aspx

=========

入口： [index.md](index.md)

