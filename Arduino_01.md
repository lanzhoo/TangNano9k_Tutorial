# 编译、链接、烧写（1）:Arduino开发环境

前面专题直接采用机器码进行编程，十分麻烦。本专题介绍使用Arduino IDE开发环境结合gcc编译工具进行软件开发的方法。

我一开始是从顾长怡先生的书籍“基于FPGA与RISC-V的嵌入式系统设计”及其配套开源代码中学习到用Arduino IDE和FPGA中的core配合的，建议大家也买本来学习。

Arduino一开始代表一系列的单片机开发板（典型的是Arduino UNO，其芯片是ATMega328P）及其软件开发环境（Arduino IDE）。由于Arduino IDE提供了方便的方法给其它芯片厂家以支持其它芯片的软件开发和下载，现在Arduino IDE已经不局限于为ATMega系列的芯片提供开发环境，而是支持大量的各种厂家的各种型号芯片以及支持各种指令集架构。

（假如您有使用乐鑫的esp32系列芯片，可以尝试一下esp32-arduino（https://github.com/espressif/arduino-esp32/ ））



### 开发环境安装及配置

#### Arduino IDE安装

自行搜索arduino官网及文档，下载软件并安装。

以下文字假设安装目录为“C:\Arduino”。

运行Arduino，选择菜单“文件/首选项”，勾选“显示详细输出：编译，上传”。

#### 开发板插件快速安装

我将下面配置过程的结果打包上传在单独的一个仓库里：“https://gitee.com/lanzhoo/arduino_riscvgcc_tools ”。

大家将其中的riscv.rar下载，解压到Arduino安装目录的hardware子目录下（如“C:\Arduino\hardware\”），然后将其tools下的gcc.rar解压。（压缩包可删除）

重启Arduino，在菜单“工具/开发板”中选择“Tang Nano9k Board”。

选择“文件/新建”，会出现一个新窗口，里面有空的setup()和loop()。

点击“文件”下面的勾形图标，开始编译。

窗口下面会看到编译过程。没出问题就说明环境搭建成功。


#### riscv-gcc编译工具链安装

Arduino IDE允许我们配置自定义的编译工具链，我们选择gcc for risc-v的无操作系统版本。搜索riscv-none-elf-gcc或者riscv-none-embed-gcc，可以找到gcc的源代码仓库。

我们需要的是已编译好的程序。2022.11搜到的版本及下载位置如下所述。

从网站 https://github.com/xpack-dev-tools/riscv-none-elf-gcc-xpack/releases/ 下载12.1.0-2版本for win32-x64的安装包xpack-riscv-none-elf-gcc-12.1.0-2-win32-x64.zip。（最新版本没有win32选项）

在arduino安装目录的hardware子目录下创建如下目录：
```
C:\Arduino\hardware\riscv\tangnano9k\tools
```

将xpack-riscv-none-elf-gcc-12.1.0-2-win32-x64.zip解压到该目录下。把目录改名为gcc。（为了缩短目录长度）

#### 配置文件：boards.txt和platform.txt

（配置文件boards.txt和platform.txt的原型来自顾长怡先生的书籍“基于FPGA与RISC-V的嵌入式系统设计”的配套开源代码。）

在tangnano9k目录下创建boards.txt，输入：
```
TANG.name=TANG Nano9k Board (Gaoyun GW1NR)

TANG.platform=riscv-gcc
TANG.build.board=_BOARD_TANG_NANO9K_

TANG.build.extra_flags = -march=rv32i -mabi=ilp32 
TANG.compiler.c.extra_flags= 
TANG.compiler.cpp.extra_flags= 
TANG.ld.extra_flags = 

TANG.upload.maximum_size=4096
TANG.upload.speed=115200
TANG.upload.tool=Reindeer_upload

TANG.build.mcu=tiny
TANG.build.core=riscv
TANG.build.variant=generic
```

在tangnano9k目录下创建platform.txt，输入：
```
name=Lanzhoo RISC-V (TANG Nano9k)

compiler.path           = {runtime.platform.path}/tools/gcc/bin/

compiler.define= -DIDE=Arduino "-I{compiler.path}../riscv-none-elf/include"

build.extra_flags       =
build.flags             = 

compiler.c.cmd          = riscv-none-elf-gcc
compiler.c.flags        =  -W -c --specs=nosys.specs -O1 -fdata-sections -ffunction-sections -fno-exceptions -fno-unwind-tables 
compiler.c.extra_flags  =
compiler.elf.flags      =
compiler.elf.cmd        = riscv-none-elf-gcc

compiler.ld.cmd = riscv-none-elf-gcc
compiler.ld.flags= -Wl,--gc-sections -static -lm --specs=nosys.specs
compiler.ld.extra_flags=
compiler.ldscript=Reindeer.ld

compiler.ar.cmd=riscv-none-elf-ar
compiler.ar.flags=rcs

compiler.objcopy.cmd=riscv-none-elf-objcopy

compiler.elf2hex.cmd=riscv-none-elf-objcopy

compiler.size.path = {runtime.platform.path}/tools/gcc/bin/
compiler.size.cmd = riscv-none-elf-size

build.variant=generic
build.ldscript.path={build.variant.path}

core.header=Arduino.h

compiler.c.elf.extra_flags= -I{build.core.path}
compiler.S.extra_flags=
compiler.cpp.extra_flags=
compiler.ar.extra_flags=

compiler.objcopy.eep.flags = -O binary
compiler.objcopy.eep.extra_flags=

compiler.elf2hex.flags = -O ihex
compiler.elf2hex.extra_flags=

recipe.c.o.pattern="{compiler.path}{compiler.c.cmd}" {build.flags} {compiler.c.flags} {compiler.define} {compiler.c.extra_flags} {build.extra_flags} -I{build.path}/sketch {includes} "{source_file}" -o "{object_file}"

recipe.cpp.o.pattern="{compiler.path}{compiler.c.cmd}" {build.flags} {compiler.c.flags} {compiler.define} {compiler.c.extra_flags} {build.extra_flags} -I{build.path}/sketch {includes} "{source_file}" -o "{object_file}"

recipe.ar.pattern="{compiler.path}{compiler.ar.cmd}"  {compiler.ar.flags} {compiler.ar.extra_flags} "{archive_file_path}"  "{object_file}"

recipe.c.combine.pattern="{compiler.path}{compiler.ld.cmd}" -T {build.ldscript.path}/{compiler.ldscript} {build.flags} {compiler.ld.flags} {build.extra_flags} -o "{build.path}/{build.project_name}.elf" {object_files} "{build.path}/{archive_file}"

recipe.objcopy.hex.pattern="{compiler.path}{compiler.elf2hex.cmd}" {compiler.elf2hex.flags} {compiler.elf2hex.extra_flags} "{build.path}/{build.project_name}.elf"  "{build.path}/{build.project_name}.hex"

recipe.output.tmp_file={build.project_name}.hex
recipe.output.save_file={build.project_name}.{build.variant}.hex

recipe.size.pattern="{compiler.size.path}/{compiler.size.cmd}"  "{build.path}/{build.project_name}.hex"
recipe.size.regex=Total\s+([0-9]+).*
 
# up loader
tools.Reindeer_upload.cmd=reindeer_config
tools.Reindeer_upload.path={runtime.platform.path}/tools

tools.Reindeer_upload.upload.params.verbose=
tools.Reindeer_upload.upload.params.quiet=
tools.Reindeer_upload.upload.protocol=UART

tools.Reindeer_upload.upload.pattern="{path}/{cmd}" --reset --run --port={serial.port} --baud=115200 "--image={build.path}/{build.project_name}.hex"
```

#### 链接配置文件：Reindeer.ld
（配置文件Reindeer.ld来自顾长怡先生的书籍“基于FPGA与RISC-V的嵌入式系统设计”的配套开源代码。修改了入口地址）

在tangnano9k目录下创建子目录variants\generic，在generic下创建Reindeer.ld文件，文件具体内容请查看仓库文件。（“https://gitee.com/lanzhoo/arduino_riscvgcc_tools ”）

#### 上传程序：reindeer_config.exe
（上传程序reindeer_config.exe来自顾长怡先生的书籍“基于FPGA与RISC-V的嵌入式系统设计”的配套开源代码。）

从仓库（“https://gitee.com/lanzhoo/arduino_riscvgcc_tools ”）下载riscv.rar，在其中的riscv\tangnano9k\tools下找到reindeer_config.exe，放置在tangnano9k的tools子目录下。

#### 主程序代码：main.cpp和Arduino.h
在tangnano9k目录下创建子目录cores\riscv，在riscv下创建main.c文件，输入：
```
#include "Arduino.h"
int main(){          
    setup();
    while (1) {
        loop();
    } 
    return 0;
}
```

在riscv下创建Arduino.h文件，输入：
```
#ifndef _ARDUINO_H
#define _ARDUINO_H

#ifdef  __cplusplus
extern "C" {
#endif

extern void setup();
extern void loop();

#ifdef  __cplusplus
}
#endif

#endif
```

（注：这里涉及.c和.cpp中函数相互调用的链接问题。如果Arduino.h中不加“#ifdef  __cplusplus”相关语句，则main.c中的main()在链接时找不到.ino.cpp中实现的setup()和loop()。（把main.c改名为main.cpp就可以））

#### 测试

重启Arduino，在菜单“工具/开发板”中选择“Tang Nano9k Board”。

选择“文件/新建”，会出现一个新窗口，里面有空的setup()和loop()。

点击“文件”下面的勾形图标，开始编译。

窗口下面会看到编译过程。没出问题就说明环境搭建成功。

#### 删减工具链文件

工具链压缩包有，展开有差不多2GB。其中许多文件用不上，可以删除。

大家可以参照仓库arduino_riscvgcc_tools的内容，对照查看删除了哪些文件。

清理后的tools/gcc目录约80MB。

下来编译时若遇到找不到的库或头文件，再从工具链压缩包中获取。

#### Arduino编译过程简单分析

将Arduino IDE编译时输出的日志复制出来分析：（“文件/首选项”中要勾选“显示详细输出：编译，上传”）
```

C:\Arduino\arduino-builder -dump-prefs -logger=machine -hardware C:\Arduino\hardware -hardware C:\Users\ThinkPad\AppData\Local\Arduino15\packages -tools C:\Arduino\tools-builder -tools C:\Arduino\hardware\tools\avr -tools C:\Users\ThinkPad\AppData\Local\Arduino15\packages -built-in-libraries C:\Arduino\libraries -libraries C:\Users\ThinkPad\Documents\Arduino\libraries -fqbn=riscv:tangnano9k:TANG -ide-version=10812 -build-path C:\Users\ThinkPad\AppData\Local\Temp\arduino_build_268621 -warnings=none -build-cache C:\Users\ThinkPad\AppData\Local\Temp\arduino_cache_579710 -prefs=build.warn_data_percentage=75 -verbose C:\hzheng\TangNano9k\Arduino\Blink1\Blink1.ino

C:\Arduino\arduino-builder -compile -logger=machine -hardware C:\Arduino\hardware -hardware C:\Users\ThinkPad\AppData\Local\Arduino15\packages -tools C:\Arduino\tools-builder -tools C:\Arduino\hardware\tools\avr -tools C:\Users\ThinkPad\AppData\Local\Arduino15\packages -built-in-libraries C:\Arduino\libraries -libraries C:\Users\ThinkPad\Documents\Arduino\libraries -fqbn=riscv:tangnano9k:TANG -ide-version=10812 -build-path C:\Users\ThinkPad\AppData\Local\Temp\arduino_build_268621 -warnings=none -build-cache C:\Users\ThinkPad\AppData\Local\Temp\arduino_cache_579710 -prefs=build.warn_data_percentage=75 -verbose C:\hzheng\TangNano9k\Arduino\Blink1\Blink1.ino

Using board 'TANG' from platform in folder: C:\Arduino\hardware\riscv\tangnano9k
Using core 'riscv' from platform in folder: C:\Arduino\hardware\riscv\tangnano9k

Detecting libraries used...
"C:\\Arduino\\hardware\\riscv\\tangnano9k/tools/gcc/bin/riscv-none-elf-gcc" -W -c --specs=nosys.specs -O1 -fdata-sections -ffunction-sections -fno-exceptions -fno-unwind-tables -DIDE=Arduino "-IC:\\Arduino\\hardware\\riscv\\tangnano9k/tools/gcc/bin/../riscv-none-elf/include" -march=rv32i -mabi=ilp32 "-IC:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621/sketch" "-IC:\\Arduino\\hardware\\riscv\\tangnano9k\\cores\\riscv" "-IC:\\Arduino\\hardware\\riscv\\tangnano9k\\variants\\generic" "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621\\sketch\\Blink1.ino.cpp" -o nul

Generating function prototypes...
"C:\\Arduino\\hardware\\riscv\\tangnano9k/tools/gcc/bin/riscv-none-elf-gcc" -W -c --specs=nosys.specs -O1 -fdata-sections -ffunction-sections -fno-exceptions -fno-unwind-tables -DIDE=Arduino "-IC:\\Arduino\\hardware\\riscv\\tangnano9k/tools/gcc/bin/../riscv-none-elf/include" -march=rv32i -mabi=ilp32 "-IC:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621/sketch" "-IC:\\Arduino\\hardware\\riscv\\tangnano9k\\cores\\riscv" "-IC:\\Arduino\\hardware\\riscv\\tangnano9k\\variants\\generic" "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621\\sketch\\Blink1.ino.cpp" -o "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621\\preproc\\ctags_target_for_gcc_minus_e.cpp"

"C:\\Arduino\\tools-builder\\ctags\\5.8-arduino11/ctags" -u --language-force=c++ -f - --c++-kinds=svpf --fields=KSTtzns --line-directives "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621\\preproc\\ctags_target_for_gcc_minus_e.cpp"

正在编译项目...
"C:\\Arduino\\hardware\\riscv\\tangnano9k/tools/gcc/bin/riscv-none-elf-gcc" -W -c --specs=nosys.specs -O1 -fdata-sections -ffunction-sections -fno-exceptions -fno-unwind-tables -DIDE=Arduino "-IC:\\Arduino\\hardware\\riscv\\tangnano9k/tools/gcc/bin/../riscv-none-elf/include" -march=rv32i -mabi=ilp32 "-IC:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621/sketch" "-IC:\\Arduino\\hardware\\riscv\\tangnano9k\\cores\\riscv" "-IC:\\Arduino\\hardware\\riscv\\tangnano9k\\variants\\generic" "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621\\sketch\\Blink1.ino.cpp" -o "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621\\sketch\\Blink1.ino.cpp.o"

Compiling libraries...

Compiling core...
"C:\\Arduino\\hardware\\riscv\\tangnano9k/tools/gcc/bin/riscv-none-elf-gcc" -W -c --specs=nosys.specs -O1 -fdata-sections -ffunction-sections -fno-exceptions -fno-unwind-tables -DIDE=Arduino "-IC:\\Arduino\\hardware\\riscv\\tangnano9k/tools/gcc/bin/../riscv-none-elf/include" -march=rv32i -mabi=ilp32 "-IC:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621/sketch" "-IC:\\Arduino\\hardware\\riscv\\tangnano9k\\cores\\riscv" "-IC:\\Arduino\\hardware\\riscv\\tangnano9k\\variants\\generic" "C:\\Arduino\\hardware\\riscv\\tangnano9k\\cores\\riscv\\main.c" -o "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621\\core\\main.c.o"

"C:\\Arduino\\hardware\\riscv\\tangnano9k/tools/gcc/bin/riscv-none-elf-ar" rcs "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621\\core\\core.a" "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621\\core\\main.c.o"
Archiving built core (caching) in: C:\Users\ThinkPad\AppData\Local\Temp\arduino_cache_579710\core\core_riscv_tangnano9k_TANG_24b5083064631401fd1320b227282801.a

Linking everything together...
"C:\\Arduino\\hardware\\riscv\\tangnano9k/tools/gcc/bin/riscv-none-elf-gcc" -T "C:\\Arduino\\hardware\\riscv\\tangnano9k\\variants\\generic/Reindeer.ld" -Wl,--gc-sections -static -lm --specs=nosys.specs -march=rv32i -mabi=ilp32 -o "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621/Blink1.ino.elf" "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621\\sketch\\Blink1.ino.cpp.o" "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621/core\\core.a"

"C:\\Arduino\\hardware\\riscv\\tangnano9k/tools/gcc/bin/riscv-none-elf-objcopy" -O ihex "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621/Blink1.ino.elf" "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621/Blink1.ino.hex"

"C:\\Arduino\\hardware\\riscv\\tangnano9k/tools/gcc/bin//riscv-none-elf-size" "C:\\Users\\ThinkPad\\AppData\\Local\\Temp\\arduino_build_268621/Blink1.ino.hex"
项目使用了 0 字节，占用了 (0%) 程序存储空间。最大为 4096 字节。
```

其中包含的主要过程如下：
- arduino-builder -dump-prefs：不了解；
- arduino-builder -compile：将ino代码片段文件生成cpp文件（Blink1.ino -> Blink1.ino.cpp）；
- Detecting libraries used... riscv-none-elf-gcc -c -o nul：不了解，估计是测试一下开发环境配置有没有问题；
- Generating function prototypes... （riscv-none-elf-gcc和ctags）：不了解；
- 正在编译项目...：riscv-none-elf-gcc -c -o ino.o：将ino.cpp编译为.o文件；
- Compiling libraries...：编译tangnano9k下libraries子目录下的文件；（现在没有该子目录）
- Compiling core...：先是用“riscv-none-elf-gcc -c -o”编译tangnano9k下cores子目录下的所有文件（现在只有一个main.c），然后用riscv-none-elf-ar将对应的所有.o文件打包为core.a文件（另外生成一个cache的.a文件（不了解怎么用））；
- Linking everything together...：用“riscv-none-elf-gcc -TReindeer.ld”将.ino.cpp.o和core.a链接为.elf文件；
- 生成.hex文件：用“riscv-none-elf-objcopy -O ihex”从elf文件生成hex文件；
- 显示size：riscv-none-elf-size，显示结果有问题；

生成的.hex文件将用于烧写。

生成的.elf文件可以通过riscv-none-elf-objdump.exe反编译为汇编。

注意：Arduino IDE的编译生成结果放在Temp目录下（具体目录可以从日志中截取），退出Arduino时该目录往往会被删除，所以需要时要做好备份。




