module top (
    input sys_clk,          // clk input  27MHz
    input sys_rst_n,        // button as reset
    input button1,          // user button
    input uart_rx,          // usb uart
    output uart_tx,         // usb uart
    output reg [5:0] led    // 6 LEDS pin
);

reg [23:0] counter;

always @(posedge sys_clk or negedge sys_rst_n) begin
    if (!sys_rst_n)
        counter <= 24'd0;
    else if (counter < 24'd1349_9999)       // 0.5s delay
        counter <= counter + 1'd1;
    else
        counter <= 24'd0;
end


always @(posedge sys_clk or negedge sys_rst_n) begin
    if (!sys_rst_n)
        led <= 6'b111100;
    else if (counter == 24'd1349_9999)       // 0.5s delay
        led[5:0] <= {led[4:0],led[5]};
    else
        led <= led;
end

  wire uart_tx_en = (counter == 24'b0);
  wire [31:0] uart_tx_data = {24'b0, 2'b0, led};

  wire [31:0] uart_rx_data;

 simpleuart u_simpleuart (
   .clk(sys_clk),
   .resetn(sys_rst_n),
   .ser_tx(uart_tx),
   .ser_rx(uart_rx),
   .reg_dat_we(uart_tx_en),
   .reg_dat_di(uart_tx_data),
   .reg_dat_re(1'b0),
   .reg_dat_do(uart_rx_data)
 );

endmodule